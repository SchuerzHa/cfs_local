# This file is used to define the CI pipeline for GitLab
# https://docs.gitlab.com/ee/ci/yaml
#
# Syntax tipps:
# * check on https://cfs-dev.mdmt.tuwien.ac.at/ci/lint
# * do not use a command `echo "warning: the : is not allowed in a string"`

# https://docs.gitlab.com/ee/ci/variables/#variables-expressions
variables:
  GIT_STRATEGY: fetch # this is faster than clone and should re-use a workspace
  GIT_SUBMODULE_STRATEGY: recursive
  CDASH_TRACK: Gitlab # will be overwritten by nightly schedule

# stages in the pipeline run in succession
stages:
  - check
  - build debug # build stages: debug and release
  - build release # config and build combined
  - test # we may have several test configs for one build
  - deploy

# determines when the pipeline runs
.default_pipeline_rules: &default_pipeline_rules
  only:
    refs:
      - master # for the master branch
      - /^test.*$/ # for branches/tags called "test*"
      - /^.*-test$/ # for branches/tags called "*-test"
      - web # For pipelines created using 'Run pipeline' button in GitLab UI under project/Pipelines
      - schedules # For scheduled pipelines

# cache the build files see https://docs.gitlab.com/ee/ci/caching/index.html
# use the ref-name, e.g. `master` as key -> same for sucessive builds
# it is stored as a zip archive and extracted before each stage - this takes time,
# thus we combine the build and configure step
# the key is used to define which chache is used for which job.
cache:
  key: ${CI_COMMIT_REF_NAME}
  paths:
    - build/cfsdeps/cache

#################################################
# Check STAGE
#################################################

# skip the check stage if 'skip-check' is in the commit message
.except-skip-ckeck: &except-skip-ckeck
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-check/

# Check the revision of the testsuite submodule
testsuite-revision:
  stage: check
  tags:
    - linux
  variables:
    # we clone to make sure we have everyting (including current names of the SHAs)
    GIT_STRATEGY: clone
  before_script:
    - git --no-pager log -1 --pretty=tformat:'%h %D'
    - echo "$CI_COMMIT_REF_NAME"
  script:
    # check in Testsuite submodule
    - cd Testsuite
    - git --no-pager log -1 --pretty=tformat:'%h %D'
    # check if we are on a branch called like the branch from CFS (or still on master)
    - git --no-pager log -1 --pretty=tformat:'%h %D' | egrep -w "(${CI_COMMIT_REF_NAME})|(master)"
  after_script:
    - pwd # debug
  cache: {} # disable
  allow_failure: true # it will fail if the most recent commit on the branch is not the HEAD of the branch
  <<: *default_pipeline_rules
  <<: *except-skip-ckeck

# this job is intended as a warning if the piepline has changed
# it is created only if there are changes, and will always fail (but is allowed to)
pipeline-changes:
  extends: .default_pipeline_rules
  stage: check
  tags:
    - linux
  before_script:
    - git diff HEAD~1 -- .gitlab-ci.yml
  script:
    - /the/pipeline/has/changed
  after_script: []
  allow_failure: true
  cache: {} # disable
  only:
    changes:
      - .gitlab-ci.yml
  <<: *except-skip-ckeck

# this job is intended as a warning if the piepline has been skipped by commit messages
#  
pipeline-skip:
  extends: .default_pipeline_rules
  stage: check
  tags:
    - linux
  before_script:
    - echo "$CI_COMMIT_MESSAGE" > message
    - cat message
    - sed -z 's/\n/ /g' message > message-oneline
  script:
    # print files without match (-L) and check for "message", i.e. "skip-XXX" is not in commit message 
    - grep -v skip-platform-builds message-oneline
    - grep -v skip-stable-tests message-oneline
    - grep -v skip-debug message-oneline
    - grep -v skip-testsuite-tests message-oneline
    - grep -v skip-gcc-builds message-oneline
    - grep -v skip-python-tests message-oneline
  after_script: []
  allow_failure: true
  cache: {} # disable 

runner-debug:
  extends: .default_pipeline_rules
  stage: check
  tags:
    - linux
  before_script: []
  script:
    - echo $CI_SERVER_HOST
    - echo $CI_SERVER
    - echo $GITLAB_USER_ID
    - echo $GITLAB_USER_LOGIN
    - echo $CI_RUNNER_ID
    - echo $CI_RUNNER_SHORT_TOKEN
    - echo $CI_PROJECT_NAMESPACE
    - echo $CI_PIPELINE_URL
    - echo $CI_PAGES_URL
    - echo $CI_MERGE_REQUEST_ID
    - echo $CI_JOB_URL
  after_script: []
  when: manual
  allow_failure: true
  cache: {} # disable
  <<: *except-skip-ckeck

#################################################
# BUILD STAGE
#################################################
# Here we define the build jobs, if one fails we do not need to test
    
.default_build_job: &default_build_job
  extends: .default_pipeline_rules
  script:
    # configure once to have dashboard ready
    - echo "$CFS_CONFIG_OPTIONS"
    - cmake -DBUILDNAME="$CI_COMMIT_REF_SLUG / $BUILD_NAME" -DCTEST_UPDATE_VERSION_ONLY:BOOL=ON -DBUILD_TESTING=ON -DBUILD_CPACK_BINARY=ON ${CFS_CONFIG_OPTIONS} ..
    # start cdash dashboard
    - ctest -D ExperimentalStart --track $CDASH_TRACK -V
    # update to current revision (gives link on CDash)
    - ctest -D ExperimentalUpdate --track $CDASH_TRACK -V
    # configure
    - ctest -D ExperimentalConfigure --track $CDASH_TRACK -V
    - ctest --quiet -O submit.log -D ExperimentalSubmit --track $CDASH_TRACK || cat submit.log # intermittent submit to get live CDash results
    # build
    - ctest -D ExperimentalBuild --track $CDASH_TRACK -V | tee build.out
    - ctest --quiet -O submit.log -D ExperimentalSubmit --track $CDASH_TRACK  || cat submit.log
    # install test to create arifacts
    - ctest -D ExperimentalTest -R Install --track $CDASH_TRACK # only do the install testing
    - mv "CFS-$CI_COMMIT_SHA-Linux.tar.gz" "CFS-$CI_COMMIT_REF_SLUG-Linux.tar.gz" # rename to a (branch/tag-)constant name
    # check the coding_score
    - ../ctest_scripts/shared/coding_score.sh build.out
  after_script:
    - cd build
    - ctest --quiet -O submit.log -D ExperimentalSubmit --track $CDASH_TRACK || cat submit.log # only output submit.log on fail
    # copy CMakeCache one level up to avoid conflicts with test config in artifacts
    - cp CMakeCache.txt CMakeCache_build-stage.txt
  coverage: '/coding_score=(\d+[.]\d+)/'
  artifacts:
    paths: # just an example
      - build/CFS-*-Linux.tar.gz # the packed binaries for distribution
      - build/CMakeCache_build-stage.txt
    expire_in: 3 days
  dependencies: []

.except-skip-gcc: &except-skip-gcc
  # do not add this job if commit message contains 'skip-gcc-builds'
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-gcc-builds/

.except-skip-intel: &except-skip-intel
  # do not add this job if commit message contains 'skip-intel-builds'
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-intel-builds/

.before-script-build: &before-script-build
  - mkdir -p build # create if it does not exist
  - cd build # descend in build
  - rm -f CMakeCache.txt # clear old cache

.gcc_before: &gcc_before
  before_script:
    # debug output
    - echo "GCC_VERSION=$GCC_VERSION"
    - gcc --version
    # check gcc version via grep
    - gcc --version | grep "${GCC_VERSION}\.[0-9]\.[0-9]"
    # general build before script
    - *before-script-build

.intel_before: &intel_before
  before_script:
    # debug output
    - icc -v
    - icpc -v
    - ifort -v
    # general build before script
    - *before-script-build

.max_rules: &max_rules
  only:
    variables:
      - $CI_SERVER_HOST == "cfs-dev.mdmt.tuwien.ac.at"

.max_cfs_config: &max_cfs_config
  extends: .default_build_job
  <<: *max_rules
  variables:
    MAX_CONFIG_OPTIONS: "-DUSE_IPOPT=ON -DUSE_SCPIP=ON -DUSE_SNOPT=ON -DUSE_EMBEDDED_PYTHON=ON"

      
#################################################
# BUILD DEBUG STAGE
#################################################

# skip the debug stage if 'skip-debug' is in the commit message
.except-skip-debug: &except-skip-debug
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-debug/
      - $CI_COMMIT_MESSAGE =~ /skip-gcc-builds/

# maximal build using as many options as possible
build:gcc7max-debug:
  extends: .max_cfs_config
  stage: build debug
  tags:
    - gcc7
    - license-keys
    - python
  variables:
    BUILD_NAME: "GCC7 maximal debug"
    GCC_VERSION: 7
    CFS_CONFIG_OPTIONS: "-DDEBUG=ON $MAX_CONFIG_OPTIONS"
  <<: *gcc_before
  <<: *except-skip-debug
  allow_failure: false
  when: on_success

#################################################
# BUILD RELEASE STAGE
#################################################

build:gcc5:
  stage: build release
  image: opencfs/centos6-gcc5
  tags:
    - docker
  variables:
    BUILD_NAME: "GCC5"
    GCC_VERSION: 5
  <<: *gcc_before
  <<: *default_build_job

build:gcc7:
  stage: build release
  image: opencfs/centos6-gcc7
  tags:
    - docker
  variables:
    BUILD_NAME: "GCC7"
    GCC_VERSION: 7
  <<: *gcc_before
  <<: *default_build_job
  <<: *except-skip-gcc

build:gcc7min:
  stage: build release
  image: opencfs/centos6-gcc7
  tags:
    - docker
  variables:
    BUILD_NAME: "GCC7 minimal"
    GCC_VERSION: 7
    CFS_CONFIG_OPTIONS: "-DBUILD_CFSDAT=OFF -DCFS_BLAS_LAPACK=NETLIB -DBUILD_UNIT_TESTS=OFF -DUSE_ARPACK=OFF -DUSE_CGAL=OFF -DUSE_CGNS=OFF -DUSE_ENSIGHT=OFF -DUSE_EXPRESSION_TEMPLATES=OFF -DUSE_FEAST=OFF -DUSE_FLANN=OFF -DUSE_GIDPOST=OFF -DUSE_GMSH=OFF -DUSE_GMV=OFF -DUSE_ILUPACK=OFF -DUSE_LIS=OFF -DUSE_METIS=OFF -DUSE_OPENMP=OFF -DUSE_PARDISO=OFF -DUSE_SUITESPARSE=OFF -DUSE_SUPERLU=OFF"
  <<: *gcc_before
  <<: *default_build_job
  <<: *except-skip-gcc

# maximal build using as many options as possible
build:gcc7max:
  extends: .max_cfs_config
  stage: build release
  tags:
    - gcc7
    - release
    - license-keys
    - python3.6
  variables:
    BUILD_NAME: "GCC7 maximal"
    GCC_VERSION: 7
    CFS_CONFIG_OPTIONS: $MAX_CONFIG_OPTIONS
  <<: *gcc_before
  # don't merge default_build_job and max_rules (already included in .max_cfs_config)
  <<: *except-skip-gcc

.build:gcc9:
  stage: build release
  image: opencfs/centos6-gcc9
  tags:
    - docker
  variables:
    BUILD_NAME: "GCC9"
    GCC_VERSION: 9
  <<: *gcc_before
  <<: *default_build_job
  <<: *except-skip-gcc
    
build:gcc10:
  stage: build release
  image: opencfs/fedora32
  tags:
    - docker
  variables:
    BUILD_NAME: "GCC10"
    GCC_VERSION: 10
  <<: *gcc_before
  <<: *default_build_job
  <<: *except-skip-gcc

# intel compiler builds
build:intel:
  stage: build release
  image: opencfs/centos7-intel
  tags:
    - docker
  variables:
    BUILD_NAME: "Intel2021"
  <<: *intel_before
  <<: *default_build_job
  <<: *except-skip-intel

# build with ninja
build:gcc10:ninja:
  stage: build release
  image: opencfs/fedora32
  tags:
    - docker
  before_script:
    - mkdir -p build # create if it does not exist
    - cd build # descend in build
  script:
    # configure with correct generator
    - cmake -G Ninja ..
    # build in 2 steps necessary
    - ninja -j1 cfsdeps
    - ninja
    # create artifacts
    - cpack -G TGZ
  after_script:
    - ls -la
  <<: *default_pipeline_rules
  when: manual
  artifacts:
    paths:
      - build/CFS-*-Linux.tar.gz # the packed binaries for distribution
    expire_in: 1 days
  dependencies: []


# Template for the platform builds
.platform:image:
  stage: build release
  tags:
    - docker
  after_script: []
  # include the default pipeline rules
  extends: .default_pipeline_rules
  # add an additional rule:
  # we only run this 
  #  1) if the variable $PLATFORM is present, and
  #  2) the variable $IMAGE (defined in jobs) matches the pattern in $PLATFORM
  # e.g. set PLATFORM=/(centos)|(ubuntu)/ to test centos and ubuntu
  # for further details see https://docs.gitlab.com/ee/ci/variables/README.html#environment-variables-expressions
  only:
    variables:
      - $PLATFORM && $IMAGE =~ $PLATFORM
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-platform-builds/
  # disable artifacts and cache
  artifacts: {}
  cache: {}
  # switch off cloning the Testsuite submodule
  variables:
    GIT_SUBMODULE_STRATEGY: none
  # install dependecies in the docker image
  before_script:
    # ensure build does not exist
    - rm -rf build
    # debug output
    - echo "IMAGE=$IMAGE"
    - echo "IMAGE=$TAG"
    - echo "PLATFORM=$PLATFORM"
    - cat /etc/*release
    # run the setup instructions for this IMAGE
    - ./share/scripts/mdsh --eval "share/doc/developer/build-dependencies/${IMAGE}_${TAG}.md" | bash -e -x
  # run the README which contains build instructions
  script:
    - ./share/scripts/mdsh --eval 'README.md' | bash -e -x

# add all platforms we can build on. In order to actually activate them set PLATFORM accordlingly (see above)
# the IMAGE variable should be set according to the 'image' (docker runner option) and requires
# a correctly named instruction in share/doc/developer/build-dependencies
platform:fedora:
  extends: .platform:image
  image: fedora
  variables:
    IMAGE: "fedora"
    TAG: "latest"

# the latest LTS release
platform:ubuntu:
  extends: .platform:image
  image: ubuntu
  variables:
    IMAGE: "ubuntu"
    TAG: "latest"

# the latest stable release, regardless of LTS status
platform:ubuntu:rolling:
  extends: .platform:image
  image: ubuntu:rolling
  variables:
    IMAGE: "ubuntu"
    TAG: "rolling"

# older ubuntu versions
platform:ubuntu:16.04:
  extends: .platform:image
  image: ubuntu:16.04
  variables:
    IMAGE: "ubuntu"
    TAG: "16.04"

platform:ubuntu:18.04:
  extends: .platform:image
  image: ubuntu:18.04
  variables:
    IMAGE: "ubuntu"
    TAG: "18.04"

platform:debian:
  extends: .platform:image
  image: debian
  variables:
    IMAGE: "debian"
    TAG: "latest"

platform:centos:
  extends: .platform:image
  image: centos
  variables:
    IMAGE: "centos"
    TAG: "latest"
    CMAKE_CXX_FLAGS: "-lrt"

platform:centos:7:
  extends: .platform:image
  image: centos:7
  variables:
    IMAGE: "centos"
    TAG: "7"

#################################################
# TEST STAGE
#################################################
# the test stage might run on a different runner than the build, thus
# the binaries are shared via the gitlab server (artifacts/dependencies)
#
# We have the following different test configurations
# * stable - runs the Testsuite on a single thread and disabling some unstable tests (and python tests)
# * testsuite - runs the Testsuite on all available cores (disabling python tests)
# * python - runs the Python-Tests of the testsuite
# Both _stable_ and _testsuite_ are run for all build configs (GCCx),
# _python_ is seperate as not to re-run python tests for all GCCx versions.

# test for cmake or install it
.cmake_test_or_install: &cmake_test_or_install
  - cmake --version || ls -l cmake || ( wget https://cmake.org/files/v3.10/cmake-3.10.3-Linux-x86_64.sh && mkdir $(pwd)/cmake && sh cmake-3.10.3-Linux-x86_64.sh --prefix=$(pwd)/cmake --skip-license)
  - cmake --version || export PATH=$(pwd)/cmake/bin:$PATH
  - echo $PATH
  - ctest --version
  - cmake --version

.after_cdash_submit: &after_cdash_submit
  after_script:
    - cd build
    # optionally install cmake if not present
    - *cmake_test_or_install
    # CDash submit
    - ctest --quiet -O submit.log -D ExperimentalSubmit --track $CDASH_TRACK || cat submit.log # only output submit.log on fail

# test preparation starts by extracting the build files from assets
.ctest_default: &ctest_default
  extends: .default_pipeline_rules
  # disable if a) we have no gcc builds
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-gcc-builds/
  before_script:
    - mkdir -p build
    - cd build # descend in build
    - ls # debug
    # install CFS
    - tar -xzvf "CFS-$CI_COMMIT_REF_SLUG-Linux.tar.gz" --strip-components=1 # extract to correct location
    # obtain the build config
    - ./bin/cfs --dependencies config.cmake
    # optionally install cmake if not present
    - *cmake_test_or_install
# [global setting](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-parsing) applies to last test only
# https://docs.gitlab.com/ee/ci/yaml/#coverage
# in the format '/REGEX/' - one can test REGEX at http://rubular.com/
  coverage: '/(\d+\%)\s+tests\s+passed,\s+\d+\s+tests?\s+failed\s+out\s+of\s+\d+/'
  # after-script
  <<: *after_cdash_submit
  # cache the local cmake install
  cache:
    key: cmake
    paths:
      - build/cmake

# this is the config that has to pass (excludes PYTHON since they do not need to be tested for every GCCx)
.test_stable: &test_stable
  <<: *ctest_default
  # disable also if 'skip-stable-tests' is contained in commit message
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-gcc-builds/
      - $CI_COMMIT_MESSAGE =~ /skip-stable-tests/
  script:
    # configure
    - cmake -DCTEST_UPDATE_VERSION_ONLY:BOOL=ON -DBUILDNAME="$CI_COMMIT_REF_SLUG / ${BUILD_NAME} / stable" -DBUILD_TESTING=ON -DCFS_BUILD_CONFIG_FILE=$(pwd)/config.cmake -DPIPELINE_TEST_STAGE_CONFIG=ON -DTESTSUITE_DIR=$(pwd)/../Testsuite -DTESTSUITE_PYTHON=OFF $TEST_CONFIG_OPTIONS -DTESTSUITE_THREADS=1 ..
    # start the testing
    - ctest -D ExperimentalStart --track $CDASH_TRACK # start a new dashboard
    - ctest -D ExperimentalUpdate --track $CDASH_TRACK # SHA is visible in CDash
    - ctest -D ExperimentalConfigure --track $CDASH_TRACK # show config
    - ctest -D ExperimentalTest -E "Instal|ElecTherm_ElTh_NMGtransient|CFSdat_FFT_2D|CFSdat_cell2Node_3D_Ensight|curlcurlA3d|max_ortho_robust|max_e11_iso|Optimization_Dynamic_eigenfrequency|nrfcommon|1dOscillatorQEVP|DampedBeam2D|Coil3D_Scalar_FixpointB_bestResidualLinesearch|PreStress3dEV|BoundaryAVExcitation|RotationNCAcouPotential|Abc2d|Abc2dcsvt|python_ocm" --track $CDASH_TRACK # exclude the install step and the failing tests

# test everything (but python, which is tested seperately)
.test_all_multicore: &test_all_multicore
  <<: *ctest_default
  # disable also if 'skip-testsuite-tests' is contained in commit message
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-gcc-builds/
      - $CI_COMMIT_MESSAGE =~ /skip-testsuite-tests/
  script:    
    - cmake -DCTEST_UPDATE_VERSION_ONLY:BOOL=ON -DBUILDNAME="$CI_COMMIT_REF_SLUG / ${BUILD_NAME} / all_multicore" -DBUILD_TESTING=ON -DCFS_BUILD_CONFIG_FILE=$(pwd)/config.cmake -DPIPELINE_TEST_STAGE_CONFIG=ON -DTESTSUITE_DIR=$(pwd)/../Testsuite -DTESTSUITE_PYTHON=OFF $TEST_CONFIG_OPTIONS .. # multicore = default
    - ctest -D ExperimentalStart --track $CDASH_TRACK # start a new dashboard
    - ctest -D ExperimentalUpdate --track $CDASH_TRACK # SHA is visible in CDash
    - ctest -D ExperimentalConfigure --track $CDASH_TRACK # show config
    - ctest -D ExperimentalTest -E Install --track $CDASH_TRACK # exclude the install step
  
# now we define the actual tests, i.e. we decide which build we want to test
# the build is taken from the dependecies
stable:gcc5:linux:
  tags:
    - linux
  image: opencfs/ubuntu20.04
  stage: test
  variables:
    BUILD_NAME: "GCC5"
  retry: 1
  <<: *test_stable
  dependencies:
    - build:gcc5

testsuite:gcc5:linux:
  tags:
    - linux
  stage: test
  variables:
    BUILD_NAME: "GCC5"
  <<: *test_all_multicore
  allow_failure: true # we know some tests will fail on multi-core runs, so this will not stop the pipeline
  dependencies:
    - build:gcc5

stable:gcc7min:linux:
  tags:
    - linux
  image: opencfs/ubuntu20.04
  stage: test
  variables:
    BUILD_NAME: "GCC7 minimal"
  retry: 1
  <<: *test_stable
  dependencies:
    - build:gcc7min

testsuite:gcc7min:linux:
  tags:
    - linux
  stage: test
  variables:
    BUILD_NAME: "GCC7 minimal"
  <<: *test_all_multicore
  allow_failure: true # we know some tests will fail on multi-core runs, so this will not stop the pipeline
  dependencies:
    - build:gcc7min

stable:gcc7max:linux:
  tags:
    - linux
    - python3.6
  stage: test
  <<: *max_rules
  variables:
    BUILD_NAME: "GCC7 maximal"
  <<: *test_stable
  dependencies:
    - build:gcc7max

testsuite:gcc7max:linux:
  tags:
    - linux
    - python3.6
  stage: test
  <<: *max_rules
  variables:
    BUILD_NAME: "GCC7 maximal"
  <<: *test_all_multicore
  allow_failure: true # we know some tests will fail on multi-core runs, so this will not stop the pipeline
  dependencies:
    - build:gcc7max

.stable:gcc7max-debug:linux:
  tags:
    - linux
  stage: test
  <<: *max_rules
  variables:
    BUILD_NAME: "GCC7 maximal debug"
    TEST_CONFIG_OPTIONS: "-DTESTSUITE_TIMEOUT=1500"
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-gcc-builds/
      - $CI_COMMIT_MESSAGE =~ /skip-testsuite-tests/
      - $CI_COMMIT_MESSAGE =~ /skip-debug/
  retry: 1
  <<: *test_stable
  needs:
    - build:gcc7max-debug

.stable:gcc9:linux:
  tags:
    - linux
  stage: test
  variables:
    BUILD_NAME: GCC9
  retry: 1
  <<: *test_stable
  dependencies:
    - build:gcc9

stable:gcc10:linux:
  image: opencfs/fedora32
  tags:
    - linux
    - docker
  stage: test
  variables:
    BUILD_NAME: GCC10
  retry: 1
  <<: *test_stable
  dependencies:
    - build:gcc10

testsuite:gcc10:linux:
  image: opencfs/fedora32
  tags:
    - linux
    - docker
  stage: test
  variables:
    BUILD_NAME: GCC10
  <<: *test_all_multicore
  allow_failure: true # we know some tests will fail on multi-core runs, so this will not stop the pipeline
  dependencies:
    - build:gcc10

stable:intel:linux:
  tags:
    - linux
  image: opencfs/ubuntu20.04
  stage: test
  variables:
    BUILD_NAME: Intel
  <<: *test_stable
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-intel-builds/
      - $CI_COMMIT_MESSAGE =~ /skip-stable-tests/
  dependencies:
    - build:intel

testsuite:intel:linux:
  tags:
    - linux
  stage: test
  variables:
    BUILD_NAME: Intel
  <<: *test_all_multicore
  allow_failure: true
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-intel-builds/
      - $CI_COMMIT_MESSAGE =~ /skip-testsuite-tests/
  dependencies:
    - build:intel

python:stable:
  image: opencfs/centos6
  extends: .ctest_default
  # disable if 'skip-python-tests' is found in commit message
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip-python-tests/
  tags:
    - python
  stage: test
  variables:
    TEST_NAME: "stable"
    EXCLUDE: "nrfcommon"
  before_script:
    - mkdir -p build
    - cd build
    - *cmake_test_or_install # optionally install cmake if not present
    - cmake -DBUILDNAME="$CI_COMMIT_REF_SLUG / python / $TEST_NAME" -DCTEST_UPDATE_VERSION_ONLY=ON -DBUILD_TESTING=ON -DPIPELINE_TEST_STAGE_CONFIG=ON -DTESTSUITE_DIR=$(pwd)/../Testsuite -DTESTSUITE_PYTHON=ON ..
  script:
    - ctest -D ExperimentalStart --track $CDASH_TRACK # start a new dashboard
    - ctest -D ExperimentalUpdate --track $CDASH_TRACK # SHA is visible in CDash
    - ctest -D ExperimentalConfigure --track $CDASH_TRACK # show config
    - ctest -D ExperimentalTest -R PYTHON -E "$EXCLUDE" --track $CDASH_TRACK # only test python
  <<: *after_cdash_submit

python:all:
  extends: python:stable
  variables:
    TEST_NAME: "all"
    EXCLUDE: ""
  allow_failure: true
  
#################################################
# DEPLOY STAGES
#################################################

.deploy_rules: &deploy_rules
  only:
    refs:
      - master # only for refs pushed to master
    variables:
      # use variables to limit deply jobs for mdmt to pipelines from the corresponding namespace
      # see the default variales https://docs.gitlab.com/ee/ci/variables/#predefined-variables-environment-variables
      - $CI_PROJECT_NAMESPACE == "cfs"
  tags:
    - linux
    - mdmt
  stage: deploy
  cache: {}
  after_script: []
  before_script: []

pages:
  stage: deploy
  tags:
    - linux
  dependencies:
    - build:gcc5
  script:
    - tar -xzvf "build/CFS-$CI_COMMIT_REF_SLUG-Linux.tar.gz" # extract the build
    - mkdir -p public
    - mv "build/CFS-$CI_COMMIT_REF_SLUG-Linux.tar.gz" "public/." # move to public
    - cp -r "CFS-$CI_COMMIT_SHA-Linux/share/xml" "public/." # publish XML scheme
  artifacts:
    paths:
      - public
  only:
    refs:
      - master # only for refs pushed to master
    variables:
      # only deploy on gitlab.com
      - $CI_PROJECT_NAMESPACE == "openCFS"
  cache: {}
  after_script: []
  before_script: []

copy_to_draco:
  dependencies:
    - build:gcc5
  script:
    # copy over via scp
    - scp "build/CFS-$CI_COMMIT_REF_SLUG-Linux.tar.gz" "draco:/opt/programs/cfs_master/CFS-$CI_COMMIT_SHA-Linux.tar.gz"
  environment:
    name: share_programs
  <<: *deploy_rules

install_on_draco:
  dependencies: []
  script:
    # wait for the copy to finish, timeout after 180s, if timeout try to list file
    - ssh draco "inotifywait -t180 -e close_write /opt/programs/cfs_master" || ssh draco "ls /opt/programs/cfs_master/CFS-$CI_COMMIT_SHA-Linux.tar.gz"
    # extract on draco by ssh
    - ssh draco "tar -xzf /opt/programs/cfs_master/CFS-$CI_COMMIT_SHA-Linux.tar.gz -C /opt/programs/cfs_master"
    # set the new symlink and replace the old one in one atomic operation
    - ssh draco "cd /opt/programs/cfs_master/; ln -s CFS-$CI_COMMIT_SHA-Linux new; mv -fT new latest"
    # run the cleanup script
    - ssh draco "cd /opt/programs/cfs_master/; ./cleanup.sh"
  environment:
    name: share_programs
  <<: *deploy_rules
