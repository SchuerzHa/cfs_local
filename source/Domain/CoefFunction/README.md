CoefFunction  ([back to main page](/source/CFS_Library_Documentation.md))
===

The CoefFunction concept is based on the idea, that it represents given data

* It can depend on space and time 

* High level of abstraction 

* It can represent materials, external fields or even as an interface to an FeFunction for non-linear and iteratively coupled PDEs 

* Basic functionality: Return its scalar/vectorial/tensorial value at the given integration point 

>

Link to [CoefFunction-doxygen](https://cfs.mdmt.tuwien.ac.at/docu/doxygen/html/classCoupledField_1_1CoefFunction.html); for the methods see [CoefFunction-Source](/source/Domain/CoefFunction/CoefFunction.hh)
