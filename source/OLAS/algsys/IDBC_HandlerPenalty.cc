#include "IDBC_HandlerPenalty.hh"

#include "MatVec/SBM_Matrix.hh"
#include "MatVec/SBM_Vector.hh"

#include "OLAS/algsys/AlgebraicSys.hh"
#include "DataInOut/Logging/LogConfigurator.hh"
#include <boost/type_traits/is_complex.hpp>

DEFINE_LOG(idbcPenalty, "idbcPenalty")

namespace CoupledField {

  template <typename T>
  IDBC_HandlerPenalty<T>::IDBC_HandlerPenalty( StdVector<UInt> numIDBC ) {

    // Initialise penalty term
    penaltyTerm_ = 0.0;

    // Determine entry type from template parameter
    eType_ = boost::is_complex<T>() ? BaseMatrix::COMPLEX : BaseMatrix::DOUBLE;

    // Generate vector for storing Dirchlet values
    SingleVector *stdVec = NULL;
    BaseVector *bVec = NULL;
    SBM_Vector *sbmVec = new SBM_Vector( numIDBC.GetSize() );
    for ( UInt i = 0; i < numIDBC.GetSize(); i++ ) {

      // If there are IDBCs generate and insert sub-vector
      //
      // NOTE: We use SPARSE_NONSYM as matrix storage type in order to
      //       obtain a Vector<T> object
      if ( numIDBC[i] > 0 ) {
        bVec = GenerateSingleVectorObject( eType_, numIDBC[i]);
        stdVec = dynamic_cast<SingleVector*>(bVec);
        sbmVec->SetSubVector( stdVec, i );
      }
    }
    dirichletValue_ = sbmVec;

    // repeat to get storage for old dbc Values (for computation of delta IDBC = dirichletValue_ - oldDirichletValue_
    SBM_Vector *sbmVec2 = new SBM_Vector( numIDBC.GetSize() );
    for ( UInt i = 0; i < numIDBC.GetSize(); i++ ) {

      // If there are IDBCs generate and insert sub-vector
      //
      // NOTE: We use SPARSE_NONSYM as matrix storage type in order to
      //       obtain a Vector<T> object
      if ( numIDBC[i] > 0 ) {
        bVec = GenerateSingleVectorObject( eType_, numIDBC[i]);
        stdVec = dynamic_cast<SingleVector*>(bVec);
        sbmVec2->SetSubVector( stdVec, i );
      }
    }
    oldDirichletValue_ = sbmVec2;

  }


  // **************
  //   Destructor
  // **************
  template <typename T>
  IDBC_HandlerPenalty<T>::~IDBC_HandlerPenalty() {

    // Delete vector of Dirichlet values
    delete dirichletValue_;
    delete oldDirichletValue_;
  }


  // ***********************
  //   InitDirichletValues
  // ***********************
  template <typename T>
  void IDBC_HandlerPenalty<T>::InitDirichletValues() {
    dirichletValue_->Init();
    oldDirichletValue_->Init();
  }

  // ***********************
  //   SetOldDirichletValues
  // ***********************
  template <typename T>
  void IDBC_HandlerPenalty<T>::SetOldDirichletValues() {
    oldDirichletValue_->Init();
    oldDirichletValue_->Add(dynamic_cast<const SBM_Vector&>(*dirichletValue_));
  }

  // *********************
  //   AdaptSystemMatrix
  // *********************
  template <typename T>
  void IDBC_HandlerPenalty<T>::AdaptSystemMatrix( SBM_Matrix &sysMat ) {

    // Compute penalty term
    penaltyTerm_ = sysMat.GetMaxDiag() * 1e12;
      
    StdMatrix *stdMat = NULL;
    const unsigned int srows(sysMat.GetNumRows());
    for ( UInt i = 0; i < srows; i++ ) {
      stdMat = sysMat.GetPointer(i,i);
      if ( dirichletEqns_[i].GetSize() > 0 && stdMat != NULL ) {
        StdVector<UInt> & myEqns = dirichletEqns_[i];
        UInt numIDBC = myEqns.GetSize();
        for( UInt j = 0; j < numIDBC; ++j ) {
          stdMat->SetDiagEntry( myEqns[j]-1, T(penaltyTerm_) );  
        }
      }
    }
  }


  // ****************
  //   AddIDBCToRHS
  // ****************
  template <typename T>
  void IDBC_HandlerPenalty<T>::AddIDBCToRHS( SBM_Vector *rhs, bool deltaIDBC ) {
    

    // We need pointers to sub-vectors
    SingleVector *stdVec = NULL;
    SingleVector *stdVal = NULL;

    if(!deltaIDBC){

      // Now loop over all blocks / sub-vectors and
      // set the Dirichlet values
      T entry;
      for ( UInt i = 0; i < rhs->GetSize() ; i++ ) {

        // obtain sub-vectors
        stdVec = rhs->GetPointer( i );
        stdVal = dirichletValue_->GetPointer( i );

        // Now adapt the sub-vector
        if ( dirichletEqns_[i].GetSize() > 0
            && stdVec != NULL && stdVal != NULL ) {
          StdVector<UInt> & myEqns = dirichletEqns_[i];
          UInt numIDBC = myEqns.GetSize();

          for( UInt j = 0; j < numIDBC; j++ ) {
            stdVal->GetEntry(j, entry);
            entry *=  penaltyTerm_;
            stdVec->SetEntry( myEqns[j] - 1, entry );
          }
        }
      }
    } else {
      SingleVector *oldStdVal = NULL;

      // Now loop over all blocks / sub-vectors and
      // set the Dirichlet values
      T entry, oldEntry;
      for ( UInt i = 0; i < rhs->GetSize() ; i++ ) {

        // obtain sub-vectors
        stdVec = rhs->GetPointer( i );
        stdVal = dirichletValue_->GetPointer( i );
        oldStdVal = oldDirichletValue_->GetPointer( i );

        // Now adapt the sub-vector
        if ( dirichletEqns_[i].GetSize() > 0
            && stdVec != NULL && stdVal != NULL ) {
          StdVector<UInt> & myEqns = dirichletEqns_[i];
          UInt numIDBC = myEqns.GetSize();

          for( UInt j = 0; j < numIDBC; j++ ) {
            stdVal->GetEntry(j, entry);
            oldStdVal->GetEntry(j, oldEntry);
            entry -= oldEntry;
            entry *=  penaltyTerm_;
            stdVec->SetEntry( myEqns[j] - 1, entry );
          }
        }
      }
    }
  }


  // ***********
  //   SetIDBC
  // ***********
  template <typename T>
  void IDBC_HandlerPenalty<T>::SetIDBC( UInt rowBlock, UInt rowNum,
                                        const T &val ) {
    
    // get hold of block specific data
    std::map<Integer, UInt> & myIndices = bcIndices_[rowBlock];
    StdVector<UInt> & myEqns = dirichletEqns_[rowBlock];
    SingleVector *stdVec = dirichletValue_->GetPointer( rowBlock );
    
    // Check, if equation was already set and determine index
    UInt index = 0;
    std::map<Integer, UInt>::iterator it = myIndices.find(rowNum); 
    if( it != myIndices.end() ) {
      index = it->second; 
    } else {
      myIndices[rowNum] = myEqns.GetSize();
      index = myEqns.GetSize();
      myEqns.Push_back(rowNum);
    }
    stdVec->SetEntry(index,val);
  }
  
  // ***********
   //   GetIDBC
   // ***********
   template <typename T>
   void IDBC_HandlerPenalty<T>::GetIDBC( UInt rowBlock, UInt rowNum,
                                         T &val, bool deltaIDBC ) {
     
     // get hold of block specific data
     std::map<Integer, UInt> & myIndices = bcIndices_[rowBlock];
     SingleVector *stdVec = dirichletValue_->GetPointer( rowBlock );
     
     // Check, if equation was already set and determine index
     UInt index = 0;
     std::map<Integer, UInt>::iterator it = myIndices.find(rowNum); 
     if( it != myIndices.end() ) {
       index = it->second; 
     } else {
       EXCEPTION("Index " << rowNum << " in sbmBlock #"
                <<  rowBlock << " is no Dirichlet entry"); 
     }
     stdVec->GetEntry(index,val);

     if(deltaIDBC){
       T aux;
       stdVec = oldDirichletValue_->GetPointer( rowBlock );
       stdVec->GetEntry(index,aux);
       val -= aux;
     }
   }


  // *****************
  //   SetDofsToIDBC
  // *****************
  template <typename T>
  void IDBC_HandlerPenalty<T>::SetDofsToIDBC( SBM_Vector *vec, bool deltaIDBC ) {

    // Loop over all sub-vectors
    SingleVector *stdVec = NULL;
    SingleVector *stdVal = NULL;
    T aux;

    if(!deltaIDBC){

      for ( UInt i = 0; i < vec->GetSize(); i++ ) {
        stdVec = vec->GetPointer( i );
        stdVal = dirichletValue_->GetPointer( i );

        // leave, if no Dirichlet values are defined for this block
        if( stdVal == NULL )
          continue;

        StdVector<UInt> & myEqns = dirichletEqns_[i];

        // Insert values
        for ( UInt j = 0; j < stdVal->GetSize(); j++ ) {
          stdVal->GetEntry( j, aux );
          stdVec->SetEntry( myEqns[j]-1, aux );
        }
      }
    } else {
      SingleVector *stdVal2 = NULL;
      T aux2;

      for ( UInt i = 0; i < vec->GetSize(); i++ ) {
        stdVec = vec->GetPointer( i );
        stdVal = dirichletValue_->GetPointer( i );
        stdVal2 = oldDirichletValue_->GetPointer( i );

        // leave, if no Dirichlet values are defined for this block
        if( stdVal == NULL )
          continue;

        StdVector<UInt> & myEqns = dirichletEqns_[i];

        // Insert values
        for ( UInt j = 0; j < stdVal->GetSize(); j++ ) {
          stdVal->GetEntry( j, aux );
          stdVal2->GetEntry( j, aux2 );
          stdVec->SetEntry( myEqns[j]-1, aux - aux2 );
        }
      }
    }
  }

  // **********************
  //   PrintInternalState
  // **********************
  template <typename T>
  void IDBC_HandlerPenalty<T>::PrintInternalState( std::ostream &os ) {

    REFACTOR;

//      os << " IDBC_HandlerPenalty --- Internal State:\n"
//         << " --------------------------------------\n"
//         << " numIDBC_ = " << numIDBC_ << '\n' << std::endl;
//
//      SingleVector *stdVal = NULL;
//      SBM_Vector* sbmVec = dynamic_cast<SBM_Vector*>(dirichletValue_);
//      if(sbmVec == NULL) EXCEPTION("Invalid cast attempt!");
//      UInt aux = 0;
//      UInt idx = 0;
//      T val;
//
//      // Compute column widths for pretty-printing
//      aux = numIDBC_ + 1;
//      UInt cw1 = aux > 0 ? (UInt)std::log10( (float)aux ) + 1 : 1;
//      UInt cw2 = 5;
//      // UInt cw3 = 4; // TODO: Check if this is still needed
//
//      for ( UInt i = 0; i < dirichletValue_->GetSize(); i++ ) {
//
//        // Compute number of IDBCs
//        aux = offset_[i+1] - offset_[i];
//
//        // Get hold of sub-vector
//        stdVal = sbmVec->GetPointer(i);
//
//        // Print IDBCs for this block
//        os << " block # " << i << " / numIDBCs = " << aux << std::endl;
//        if ( aux > 0 ) {
//          os << " # | eqnNo | comp | value" << std::endl;
//          for ( UInt j = 0; j < aux; j++ ) {
//
//            stdVal->GetEntry( j, val );
//            idx = offset_[i] + j;
//
//            os << std::setfill( ' ' )
//               << std::setw(cw1) << idx << " | "
//               << std::setw(cw2) << dirichletEQN_[idx] << " | "
//               << val << std::endl;
//          }
//        }
//        os << std::endl;
//      }
//
//    os << " --------------------------------------\n" << std::endl;

  }

  // Explicit template instantiation
  #ifdef EXPLICIT_TEMPLATE_INSTANTIATION
    template class IDBC_HandlerPenalty<Double>;
    template class IDBC_HandlerPenalty<Complex>;
  #endif  
}
