#include <assert.h>
#include <cmath>
#include <stddef.h>
#include <map>
#include <ostream>
#include <string>

#include "DataInOut/Logging/LogConfigurator.hh"
#include "DataInOut/ParamHandling/ParamNode.hh"
#include "Domain/Domain.hh"
#include "Domain/ElemMapping/Elem.hh"
#include "Domain/ElemMapping/EntityLists.hh"
#include "Domain/ElemMapping/SurfElem.hh"
#include "Driver/Assemble.hh"
#include "Driver/BucklingDriver.hh"
#include "Driver/FormsContexts.hh"
#include "Forms/LinForms/LinearForm.hh"
#include "Forms/BiLinForms/BiLinearForm.hh"
#include "Forms/BiLinForms/BDBInt.hh"
#include "General/Enum.hh"
#include "General/defs.hh"
#include "General/Environment.hh"
#include "General/Exception.hh"
#include "MatVec/SingleVector.hh"
#include "MatVec/exprt/xpr1.hh"
#include "MatVec/Matrix.hh"
#include "MatVec/Vector.hh"
#include "Optimization/Design/DesignElement.hh"
#include "Optimization/Design/DesignSpace.hh"
#include "Optimization/Design/DesignStructure.hh"
#include "Optimization/Excitation.hh"
#include "Optimization/Function.hh"
#include "Optimization/OptimizationMaterial.hh"
#include "Optimization/SIMP.hh"
#include "Optimization/StressConstraint.hh"
#include "Optimization/TransferFunction.hh"
#include "PDE/SinglePDE.hh"
#include "PDE/MechPDE.hh"
#include "PDE/LatticeBoltzmannPDE.hh"
#include "Utils/StdVector.hh"
#include "Utils/mathParser/mathParser.hh"
#include "Utils/tools.hh"

namespace CoupledField {
class DenseMatrix ;
}  // namespace CoupledField

using namespace CoupledField;

using std::complex;

DEFINE_LOG(simp, "simp")


SIMP::SIMP() : ErsatzMaterial()
{
}

SIMP::~SIMP()
{
  if(mechRHS.vec != NULL)       { delete mechRHS.vec;  mechRHS.vec = NULL;  }
}

void SIMP::PostInit()
{
  ErsatzMaterial::PostInit();
  
  // FIXME
  if(context->IsComplex()) mechRHS.Init<complex<double> >(design, App::PRESSURE); // in many cases NULL;
                      else mechRHS.Init<double>(design, App::PRESSURE);
}

void SIMP::SetElementK( Function* f, DesignElement* de, const TransferFunction* tf, App::Type app, DenseMatrix* out, bool derivative, CalcMode calcMode, double ev)
{
  if(f->ctxt->IsComplex())
  {
    if(f->ctxt->mat->ComplexElementMatrix(de->elem->regionId)) // handles also bloch with real material but complex BOp
      SetElementK<Complex, Complex >( f, de, tf, app, out, derivative, calcMode, ev);
    else
      SetElementK<Complex, double >( f, de, tf, app, out, derivative, calcMode, ev);
  }
  else
    SetElementK<double,double>( f, de, tf, app, out, derivative, calcMode, ev);
}

template <class T1, class T2>
void SIMP::SetElementK(Function* f, DesignElement* de, const TransferFunction* tf, App::Type app, DenseMatrix* mat_out, bool derivative, CalcMode calcMode, double ev)
{
  assert(f->ctxt->mat != NULL);
  OptimizationMaterial* mat = f->ctxt->mat;
  Matrix<T1>& out = dynamic_cast<Matrix<T1>& >(*mat_out);
  //std::cout << "out= " << out.ToString() << std::endl;

  //assert(app != App::MAG); // shall be in MagSIMP.cc

  switch(app)
  {
  case App::MECH:
  case App::ACOUSTIC:
  case App::HEAT:
  case App::BUCKLING:
  {
    int mm = de->multimaterial != NULL ? de->multimaterial->index : -1;

    // element matrix with org material, might be cached -> local_element_cache
    const Matrix<T2>& stiffness = dynamic_cast<const Matrix<T2>& >(mat->Stiffness(de->elem, false, mm)); // no bimaterial
    LOG_DBG3(simp) << "stiffness=" << stiffness.ToString();

    // Find the transfer function for K (e.g. DENSITY, App::MECH)
    T1 k_factor = derivative ? tf->Derivative(de, DesignElement::SMART, false) : tf->Transform(de, DesignElement::SMART);// not the bimat case

    // copy from real mechStiffness to potential complex out and factor the derivative
    Assign(out, stiffness, k_factor); // out = k_factor * stiffness
    // This log is very expensive, it blows up inv_tensor in the debug mode
    // LOG_DBG3(simp) << "SetElementK: el=" << de->elem->elemNum << " di=" << de->GetIndex() << " mm=" << mm << " K_org=" <<  stiffness.ToString() << " k_factor " << k_factor << " -> " << out.ToString();

    if(design->GetRegion(de->elem->regionId)->HasBiMaterial())
    {
      const Matrix<T2>& bimat = dynamic_cast<const Matrix<T2>& >(mat->Stiffness(de->elem, true)); // yes, bimaterial
      // rho^3 * E1 + (1-rho)^3 * E2, in the derivative case 3*rho^2 * E1 - 3*(1-rho)^2 * E2
      k_factor = !derivative ? 1.0 - k_factor : -1.0 *  k_factor;
      Add(out, k_factor, bimat);
      // LOG_DBG3(simp) << "SetElementK: bimat k_factor " << k_factor << " bimat=" << bimat.ToString() << " -> " << out.ToString();

      // LOG_DBG3(simp) << "SetElementK: K_bi_org=" <<  bimat.ToString() << " k_factor " << k_factor << " -> " << out.ToString();
    }

    if(app == App::BUCKLING)
    {
      assert(f->ctxt->DoBuckling());

      if (f->ctxt->GetBucklingDriver()->IsInverseProblem())
        out *= -ev;

      tf = design->GetTransferFunction(de->GetType(), App::BUCKLING);
      AddGeometricStiffnessToStiffness(f->ctxt, tf, de, dynamic_cast<Matrix<Complex>& >(out), derivative, false, calcMode, ev); // no bimaterial
      // LOG_DBG3(simp) << "SetElementK: GeoStiff out -> " << out.ToString();

      if(design->GetRegion(de->elem->regionId)->HasBiMaterial())
      {
        // rho^3 * E1 + (1-rho^3) * E2, in the derivative case 3*rho^2 * E1 - 3*rho^2 * E2
        AddGeometricStiffnessToStiffness(f->ctxt, tf, de, dynamic_cast<Matrix<Complex>& >(out), derivative, true, calcMode, ev); // bimaterial
        // LOG_DBG3(simp) << "SetElementK: GeoStiff bimat out " -> " << out.ToString();
      }
    }

    if(f->ctxt->IsComplex() && !f->ctxt->DoBuckling())
    {
      tf = design->GetTransferFunction(de->GetType(), App::MASS);
      AddMassToStiffness(f->ctxt, tf, de, dynamic_cast<Matrix<complex<double> >& >(out), derivative, false, calcMode, ev); // no bimaterial
      // LOG_DBG3(simp) << "SetElementK: Mass out -> " << out.ToString();

      if(design->GetRegion(de->elem->regionId)->HasBiMaterial())
      {
        // rho^3 * E1 + (1-rho^3) * E2, in the derivative case 3*rho^2 * E1 - 3*rho^2 * E2
        AddMassToStiffness(f->ctxt, tf, de, dynamic_cast<Matrix<complex<double> >& >(out), derivative, true, calcMode, ev); // bimaterial
        // LOG_DBG3(simp) << "SetElementK: Mass bimat out " -> " << out.ToString();
      }
    }
    break;
  }

  case App::ELEC:
  {
    const Matrix<std::complex<double> >& stiffness = dynamic_cast<ElecMat *>(mat)->ElecStiffness(de->elem, false); // no bimaterial

    // Find the transfer function for K (e.g. DENSITY, App::MECH)
    T1 k_factor = derivative ? tf->Derivative(de, DesignElement::SMART) : tf->Transform(de, DesignElement::SMART);

    // copy from ElecStiffness to out and factor the derivative
    if(f->ctxt->IsComplex())
      Assign(out, dynamic_cast<const Matrix<T1>& >(stiffness), k_factor);
    else
      Assign(out, stiffness.GetPart(Global::REAL), k_factor);

    // This log is very expensive, it blows up inv_tensor in the debug mode
    // LOG_DBG3(simp) << "SetElementK: K_org=" <<  stiffness.ToString() << " k_factor " << k_factor << " -> " << out.ToString();

    if(design->GetRegion(de->elem->regionId)->HasBiMaterial())
    {
      const Matrix<std::complex<double> >& bimat = dynamic_cast<ElecMat *>(mat)->ElecStiffness(de->elem, true); // yes, bimaterial
      // rho^3 * E1 + (1-rho^3) * E2, in the derivative case 3*rho^2 * E1 - 3*rho^2 * E2
      k_factor = derivative ? tf->Derivative(de, DesignElement::SMART, true) : tf->Transform(de, DesignElement::SMART, true);
      if(f->ctxt->IsComplex())
        Add(out, k_factor, dynamic_cast<const Matrix<T1>& >(bimat));
      else
        Add(out, k_factor, bimat.GetPart(Global::REAL));
      // LOG_DBG3(simp) << "SetElementK: K_bi_org=" <<  bimat.ToString() << " k_factor " << k_factor << " -> " << out.ToString();
    }
    break;
  }

  default:
    assert(false); // other cases should be handled in PiezoSIMP
  } // end switch
}


double SIMP::CalcFunction(Excitation& excite, Function* f, bool derivative)
{
  // this app is for the PDE
  App::Type app = f->ctxt->ToApp();

  // an exceptional case where we also calculate the function value as we share a lot code with the gradient
  if(f->GetType() == Function::GLOBAL_STRESS)
    if(f->ctxt->IsComplex())
      return CalcGlobalVonMisesStress<Complex>(excite, f, derivative);
    else
      return CalcGlobalVonMisesStress<double>(excite, f, derivative);
  // microscopic load factor is interpolated value divided by local stress
  if(f->GetType() == Function::LOCAL_STRESS || f->GetType() == Function::LOCAL_BUCKLING_LOAD_FACTOR)
    return CalcLocalVonMisesStressOrLoadFactor(excite, f, derivative);

  if(!derivative)
    return ErsatzMaterial::CalcFunction(excite, f, derivative);

  // only special derivatives, the rest also EM
  switch(f->GetType())
  {
  case Function::GLOBAL_DYNAMIC_COMPLIANCE:
  case Function::OUTPUT:
  case Function::SQUARED_OUTPUT:
  case Function::DYNAMIC_OUTPUT:
  case Function::CONJUGATE_COMPLIANCE:
  case Function::ABS_OUTPUT:
  {
    // synthesis of compliant mechanism: As our adjoint PDE
    // c' = l K' u
    TransferFunction* tf = design->GetTransferFunction(DesignElement::Default(f->ctxt), TransferFunction::Default(f->ctxt), true, true); // exception and use_single
    double weight = excite.GetWeightedFactor(f);

    // squared output gradient 2 * <u,l> * <u,l>'
    if (f->GetType() == Function::SQUARED_OUTPUT)
    {
      f->SetType(Function::OUTPUT);
      weight *= 2. * SIMP::CalcFunction(excite, f, false);
      f->SetType(Function::SQUARED_OUTPUT);
    }
    LOG_DBG(simp) << "CalcFunction(idx=" << excite.index << ") norm_weight= " <<  excite.normalized_weight  << " factor=" << excite.GetFactor(f) << " weight=" << weight;
    CalcU1KU2(tf, adjoint.Get(excite, f)->elem[app], app, forward.Get(excite)->elem[app], NULL, weight, STANDARD, f);
    break;
  }

  case Function::PRESSURE_DROP:
  {
    LatticeBoltzmannPDE* lbmPde = f->ctxt->GetLatticeBoltzmannPDE();
    assert(lbmPde != NULL);
    lbmPde->SensitivityAnalysis(design->GetTransferFunction(f->elements[0]), f, design);
    break;
  }

  default:
    return ErsatzMaterial::CalcFunction(excite, f, derivative);
  }

  return 0.0; // only derivatives evaluated
}


template<class TYPE>
double SIMP::CalcGlobalVonMisesStress(Excitation& excite, Function* f, bool gradient)
{
  assert(excite.sequence == f->ctxt->sequence);

  // for the function we pack the stuff in Function::Local

  // For the function we pack the stuff in Function::Local, for the gradient we do it here as the computation are too far
  // away from the other local gradient computations.
  //
	// the gradient is lambda^T * ( K' * u - f')  + alpha * 2 * stress^T * M * (rho^p)' * E_0 * B * u
  //
  // that means, if the stress constraint region is not a design region, we don't add something. But take
  // care, there might also be several stress constraints for a set of design regions.

  StressConstraint<TYPE> sc(&excite, f, this, &forward);

  Vector<double> stress = sc.CalcStresses();

  // alpha is from the globalization which is in the form sum max(0, g_i-c)^p
  // in the gradient case alpha is p*max(0, g_i-c)^(p-1) where g_i is the vonMisesStress
  Vector<double> alpha = sc.CalcGlobalizationFactor(stress, gradient);

  if(!gradient)
  {
    // we pack the stuff to local_values
    f->GetLocal()->local_values.Import(stress.GetPointer(), stress.GetSize());
    LOG_DBG(simp) << "CGVMS: f=" << f->ToString() << " sum=" << alpha.Sum() << " alpha=" << alpha.ToString();
    return alpha.Sum();
  }
  else // gradient
  {

    // 2 * stress^T * M * (rho^p)' * E_0 * B * u
    Vector<double> appendix;

    sc.CalcGradStresses(appendix);
    assert(appendix.GetSize() == alpha.GetSize());

    // rhs is from the legacy copy and usually empty
    DesignDependentRHS rhs(App::STRESS);
    rhs.Init<double>(excite.label);

    TransferFunction* tf = design->GetTransferFunction(DesignElement::Default(f->ctxt), TransferFunction::Default(f->ctxt), true);

    // calc lambda^T *  K' * u -> this already stores the results by AddGradient()!
    CalcU1KU2(tf, adjoint.Get(excite, f)->elem[App::MECH], App::MECH, forward.Get(excite)->elem[App::MECH], &rhs, 1.0, STANDARD, f);

    // add the appendix stuff
    for(unsigned int i = 0; i < design->data.GetSize(); i++)
    {
      DesignElement& de = design->data[i];
      // Three cases:
      // a) stress is defined on whole design domain (one or more regions)
      // b) stress region is defined on one of two or more design regions
      // c) stress region is not in any of the design regions.

      // case c): idx is already -1
      int idx = -1;

      // case a) reset idx
      if(f->region == ALL_REGIONS || design->regions[0].GetSize() == 1)
      {
        assert(de.elem->elemNum == f->elements[i]->elem->elemNum);
        idx = i;
      }
      // case b) possibly reset idx
      if(idx == -1 && design->Contains(f->region))
      {
        // we are at a design element and have to find it within stress
        // TODO make it faster and check if it is right?!
        for(unsigned int e = 0; idx == -1 && e < f->elements.GetSize(); e++)
          if(de.elem->elemNum == f->elements[e]->elem->elemNum)
            idx = e;
      }

      if(idx != -1)
        de.AddGradient(f, alpha[idx] * appendix[idx]);

      LOG_DBG2(simp) << "CGVMS: f=" << f->ToString() << " de=" << de.elem->elemNum << " idx=" << idx << " alpha="
                     << (idx != -1 ? alpha[idx] : -1.0)  << "* app=" << (idx != -1 ? appendix[idx] : -1.0) << " -> " << de.GetPlainGradient(f);
  	} // end loop
    return 0.0;
  } // end gradient
}


double SIMP::CalcLocalVonMisesStressOrLoadFactor(Excitation& excite, Function* f, bool gradient)
{
  // we share a lot of code between gradient and local function value, hence share the code
  // local stress constraints are expensive and are not efficiently implemented. They are meant for a proof of concept, not for real use

  assert(excite.sequence == f->ctxt->sequence);
  assert(f->IsLocal());
  assert(f->GetCurrentRelativePosition() >= 0); // we are in a virtual loop
  assert(f->GetLocal()->virtual_elem_map.GetSize() == f->elements.GetSize());
  if(f->GetType() == Function::LOCAL_BUCKLING_LOAD_FACTOR)
  {
    if(context->ToApp() != App::MECH || f->ctxt->ToApp() != App::MECH)
      EXCEPTION("stresses are not read from static sequence step");
  }
  else
    assert(f->GetType() == Function::LOCAL_STRESS && !f->IsObjective());

  TransferFunction* tf = design->GetTransferFunction(DesignElement::Default(f->ctxt), TransferFunction::Default(f->ctxt), true);

  // For the function we pack the stuff in Function::Local, for the gradient we do it here as the computation are too far
  // away from the other local gradient computations.
  LocalCondition* lc = dynamic_cast<LocalCondition*>(f);
  Function::Local::Identifier& id = lc->GetCurrentVirtualContext();

  DesignElement* de = dynamic_cast<DesignElement*>(id.element);
  assert(de != NULL);

  assert(!f->ctxt->IsComplex()); // otherwise use template for StressConstraint

  StressConstraint<double> sc(&excite, f, this, &forward);

  // this is the squared norm of local stress (vonMises for vM stress, Euclidean for load factor)
  double val = sc.CalcElementStress(de);

  // stresses access smart, so we have to do it as well
  double vol = de->GetDesign(DesignElement::SMART);

  double ev_interpolated = GetMicroLoadFactor(vol);

  if(!gradient)
  {
    if(f->GetType() == Function::LOCAL_BUCKLING_LOAD_FACTOR)
    {
      double tmp_for_dbg = ev_interpolated / std::sqrt(val);
      // LOG_DBG3(simp) << "CLVMS: de=" << de->elem->elemNum << " rho=" << vol << " stress=" << std::sqrt(val) << " ev=" << ev_interpolated << " -> val=" << val2;
      val = tmp_for_dbg;
      int res_idx = this->GetDesign()->GetSpecialResultIndex(DesignElement::DEFAULT, DesignElement::LOCAL_LOAD_FACTOR, DesignElement::NONE, DesignElement::PLAIN, excite.label);
      // output local load factor? Note, that this is excitation specific! Only for STRESS
      if(res_idx != -1)
      {
        de->specialResult[res_idx] = val;
        // LOG_DBG3(simp) << "CLVMS: de=" << de->ToString() << " res_idx=" << res_idx << " v=" << val << " " << de->specialResult[res_idx];
      }
    }
    lc->SetValue(val); // for CalcMaxValue() used in Function::Local::Identifier::EvalFunction()
    LOG_DBG2(simp) << "CLVMS: f=" << f->ToString() << " de=" << de->elem->elemNum << " val=" << val << " values=" << f->GetLocal()->local_values.ToString();
    return val;
  }
  else
  {
    // the BaseDesignElement::constraintGradient space is not for virtual functions
    // we reuse the same space and it is written before we calculate the next function
    // this is the only case we use Reset for a specific function
    for(DesignElement& t : design->data)
      t.Reset(DesignElement::CONSTRAINT_GRADIENT, f);

    // the stress gradient is lambda^T * ( K' * u - f')  + 2 * stress^T * M * (rho^p)' * E_0 * B * u
    // f' is usually 0,
    // 2 * stress^T * M * (rho^p)' * E_0 * B * u is the dJ/drho_e part and applies only for element e

    // rhs is from the legacy copy and usually empty
    DesignDependentRHS rhs(App::STRESS);
    rhs.Init<double>(excite.label);

    StdVector<SingleVector*>& u2 = forward.Get(excite)->elem[App::MECH];

    // calc lambda^T *  K' * u -> this already stores the results by AddGradient()!
    int idx =lc->GetCurrentRelativePosition();
    StdVector<SingleVector*>& u1 = adjoint.Get(excite, f, idx)->elem[App::MECH];
    CalcU1KU2(tf, u1, App::MECH, u2, &rhs, 1.0, STANDARD, f);

    // calc 2 * stress^T * M * (rho^p)' * E_0 * B * u
    double appendix = sc.CalcGradElementStress(de);

    // this is the derivative of local stress
    de->AddGradient(f, appendix); // de is called for all f->elements, hence there is no a),b),c) case as for the global stress gradient

    // for the local load factor, we have to apply chain rule
    // we take the stress gradient, which was stored in de, apply the chain rule,
    // delete the gradient in all elements and write the new one
    if(f->GetType() == Function::LOCAL_BUCKLING_LOAD_FACTOR)
    {
      double dev_interpolated = GetMicroLoadFactor(vol, true);

      for(unsigned int e = 0; e < design->GetNumberOfElements(); e++)
      {
        DesignElement* de2 = &design->data[e];
        double dval = de2->GetPlainGradient(f); // stress gradient

        // d/dx ev/sqrt(s) = (s * ev' - s' * ev * 1/2 ) / sqrt(s) / s
        // ev is local, i.e. dev/drho is a diagonal matrix
        // however, the stress gradient is a full matrix
        double firstTerm = de->elem->elemNum == de2->elem->elemNum ? val * dev_interpolated : 0;
        appendix = (firstTerm - dval * ev_interpolated * 0.5) / std::sqrt(val) / val;

        LOG_DBG(simp) << "CLVMS: de2=" << de2->elem->elemNum << " val=" << val << " dev=" << dev_interpolated
            << " dval=" << dval << " ev=" << ev_interpolated << " -> df=" << appendix;

        de2->Reset(DesignElement::CONSTRAINT_GRADIENT, f);
        de2->AddGradient(f, appendix);
      }
    }

    LOG_DBG2(simp) << "CLVMS: f=" << f->ToString() << " de=" << de->elem->elemNum << " idx=" << idx << " appendix=" << appendix;
    return 0;
  }
}

double SIMP::GetMicroLoadFactor(double vol, bool derivative)
{
  // the tangent-function has been fitted for a triangular lattice
  // for design = 1, we have a singularity (ev -> infinity)
  // thus we cut at x0 = 0.9 and extrapolate smoothly with a quadratic function
  // TODO put function in xml and parse with mathparser
//  double factor = 1.677;
//  double exponent = 3.656;
  double factor = 1.28309;
  double exponent = 3.5642;

  // do a quadratic extrapolation
  double x0 = 0.9;
  double argument = M_PI/2 * std::pow(x0, exponent);
  double f0 = factor * std::tan(argument);
  double df0 = factor * M_PI/2 * exponent * std::pow(x0, exponent-1) / std::pow(std::cos(argument),2);
  double sum = (exponent-1) * std::pow(x0, exponent-2) + M_PI * exponent * std::pow(x0, 2*(exponent-1)) * std::tan(argument);
  double ddf0 = factor * M_PI/2 * exponent * sum / std::pow(std::cos(argument),2);

  double ev = -1;
  if(!derivative)
    if(vol < x0)
      ev = factor * std::tan(M_PI/2 * std::pow(vol, exponent));
    else
      ev = ddf0/2 * std::pow(vol-x0, 2) + df0 * (vol-x0) + f0;
  else
    if(vol < x0)
      ev = factor * M_PI/2 * exponent * std::pow(vol, exponent-1) / std::pow(std::cos(M_PI/2 * std::pow(vol, exponent)),2);
    else
      ev = ddf0 * (vol-x0) + df0;

  return ev;
}



DesignDependentRHS::DesignDependentRHS(App::Type my_app)
{
  app         = my_app;
  valid       = app == App::MAG; // MAG needs no init
  vec         = NULL;
  elem        = NULL;
  test_strain = MechPDE::NOT_SET;
  isInterfaceDriven_ = false;
}

DesignDependentRHS::~DesignDependentRHS()
{
  valid = false;
  if(vec != NULL) { delete vec; vec = NULL; }
}

template <class T>
bool DesignDependentRHS::Init(DesignSpace* design, App::Type my_app)
{
  assert(!(app != App::NO_APP && app != my_app && my_app != App::NO_APP));

  if(my_app != App::NO_APP)
    this->app = my_app;

  assert(app == App::CHARGE_DENSITY || app == App::PRESSURE || app == App::HEAT || app == App::MAG);

  if (app == App::HEAT) {
    this->valid = true;
    isInterfaceDriven_ = true;
    return true;
  }

  std::string name = app == App::CHARGE_DENSITY ? "LinNeumannInt" : "PressureLinForm";


  // check if we have a form with the application name
  LinearForm* form = NULL;
  LinearFormContext* actContext = NULL;

  SinglePDE* mech = Optimization::context->ToPDE(App::MECH, false);
  if(mech == NULL)
	return false; // wrong pde -> extend if you need it!

  StdVector<LinearFormContext*>& forms = mech->GetAssemble()->GetLinForms();

  for(StdVector<LinearFormContext*>::iterator it = forms.Begin(); it != forms.End(); it++)
  {
    // get integrator
    actContext = *it;
    if(actContext->GetIntegrator()->GetName() == name)
    {
      assert(false);
      if(form != NULL) EXCEPTION("linear surface form '" << name << "' not unique");
      form = actContext->GetIntegrator(); // form = dynamic_cast<LinearSurfForm*>(actContext->GetIntegrator());
    }
  }

  LOG_DBG(simp) << "DesignDependentRHS::Init(app = " << Optimization::application.ToString(app) << ") -> form = "
               << (form != NULL ? form->GetName() : "NULL");

  // form is not necessary defined in the xml file!
  if(form == NULL)
	return false; // no form, no RHS!

  // the context knows the surface elements!
  this->valid = true; // doubled code?!

  EntityIterator eit = actContext->GetEntities()->GetIterator();
  elem = eit.GetSurfElem();

  // FIXME
  assert(false);
  // calculate the rhs for the reference element, first store and then extract all but one node
  design->DisableTransferFunctions();
  // FIXME form->SetSurfElem(const_cast<SurfElem*>(elem)); // set the internal actElem_ of the form
  Vector<T> full;
  // FIXME form->CalcElemVector(full,const_cast<EntityIterator&>(eit));
  // enable again our transfer functions
  design->EnableTransferFunctions();


  // check the number of dofs, copy the the first nodes dofs, check that all other are the same
  assert(full.GetSize() >= elem->connect.GetSize());
  int dof = full.GetSize() / elem->connect.GetSize();
  if(dof < 0 || dof > 3)
    EXCEPTION("Surface element of " << elem->connect.GetSize() << " nodes has RHS with "
              << full.GetSize() << " entries");
  assert(vec == NULL);
  // copy the first nodes dofs
  Vector<T>* vt = new Vector<T>(dof);
  // vec is the base SingleVector which has no abstract operators overloaded
  vec = vt;
  for(int i = 0; i < dof; i++) (*vt)[i] = full[i];

  // check
  for(unsigned int n = 0; n < full.GetSize()/dof; n += dof)
    for(int d = 0; d < dof; d++)
      if(!close(full[n+d],(*vt)[d]))
        EXCEPTION("RHS values are not the same for each node: " << full.ToString());

  // store all node numbers in the sorted set
  // do at the end such iterator is valid for CalcElemVector()
  eit.Begin();
  while(!eit.IsEnd())
  {
    StdVector<unsigned int> elem_nodes = eit.GetElem()->connect;
    for(unsigned int n = 0; n < elem_nodes.GetSize(); n++)
      nodes.insert(elem_nodes[n]);
    eit++;
  }

  LOG_DBG(simp) << "DesignDependentRHS::Init -> " << ToString(1);
  LOG_DBG2(simp) << "DesignDependentRHS::Init -> " << ToString(0);

  return true;
}


template <class T>
bool DesignDependentRHS::Init(std::string excite_label, App::Type my_app)
{
  assert(!(app != App::NO_APP && app != my_app && my_app != App::NO_APP));
  if(my_app != App::NO_APP)
    app = my_app;

  assert(app == App::STRESS);
  this->test_strain = MechPDE::testStrain.IsValid(excite_label) ? MechPDE::testStrain.Parse(excite_label) : MechPDE::NOT_SET;
  return true;
}


std::string DesignDependentRHS::ToString(int level)
{
  std::ostringstream os;
  os << "valid=" << valid;
  if(!valid) return os.str();

  os << " vec=" << vec->ToString();
  os << " elem=" << elem->ToString();

  os << " nodes=";
  if(level == 0)
  {
    for(std::set<unsigned int>::iterator i = nodes.begin(); i != nodes.end(); i++)
      os << *i << ", ";
  }
  else
  {
    os << "#" << nodes.size();
  }
  return os.str();
}

// Explicit template instantiation
#ifdef EXPLICIT_TEMPLATE_INSTANTIATION
template bool DesignDependentRHS::Init<double>(DesignSpace* design, App::Type app);
template bool DesignDependentRHS::Init<complex<double> >(DesignSpace* design, App::Type app);
#endif
