SET(IDCOMP_TEMPL "${CFS_SOURCE_DIR}/share/scripts/identify_compiler.cmake.in")
SET(COMPILER_ID_FILE "${CFS_BINARY_DIR}/CMakeFiles/out.cmake")
SET(IDENTIFY_COMPILER_SRC "IdentifyCXXCompiler.cpp")

include(CheckCXXSourceCompiles)

#-------------------------------------------------------------------------------
# Determine what equivalent GNU version the compiler has, to check if it is
# compatible with the GNU C++ compiler on the system PATH.
#-------------------------------------------------------------------------------
IF(UNIX)
  #-----------------------------------------------------------------------------
  # Let's first find out some infos about the system GNU compiler first.
  #-----------------------------------------------------------------------------
  SET(COMPILER "g++")

  SET(ID_GXX "${CFS_BINARY_DIR}/share/scripts/identify_gxx.cmake")
  CONFIGURE_FILE("${IDCOMP_TEMPL}" "${ID_GXX}" @ONLY)

  EXECUTE_PROCESS(
    COMMAND "${CMAKE_COMMAND}" -E make_directory "${CFS_BINARY_DIR}/tmp"
    WORKING_DIRECTORY "${CFS_BINARY_DIR}"
    RESULT_VARIABLE RETVAL
    )

  EXECUTE_PROCESS(
    COMMAND "${CMAKE_COMMAND}" -P "${ID_GXX}"
    WORKING_DIRECTORY "${CFS_BINARY_DIR}/tmp"
    RESULT_VARIABLE RETVAL
    )

  INCLUDE("${COMPILER_ID_FILE}")

  SET(GNU_CXX_COMPILER_VER "${CXX_VERSION}")
ENDIF(UNIX)

IF(WIN32)
  #-----------------------------------------------------------------------------
    # we currently support 
    #    Visual Studio 2017 or 2019 plus Intel Fortran (2019/2020)
    # or
    #    Intel 2020 C++ and Fortran
    # the later requires either VC2017 or VC2019 environment
    # in order to build boost correctly, we need to know which env is used
    SET(CFS_MSVC_VERSION $ENV{VisualStudioVersion})
ENDIF(WIN32)



#-------------------------------------------------------------------------------
# Collect output of compiler version command to determine compiler type and
# version information.
#-------------------------------------------------------------------------------
SET(COMPILER "")

SET(ID_CXX "${CFS_BINARY_DIR}/share/scripts/identify_cxx.cmake")
CONFIGURE_FILE("${IDCOMP_TEMPL}" "${ID_CXX}" @ONLY)

EXECUTE_PROCESS(
  COMMAND "${CMAKE_COMMAND}" -P "${ID_CXX}"
  WORKING_DIRECTORY "${CFS_BINARY_DIR}/tmp"
  RESULT_VARIABLE RETVAL
  )

INCLUDE("${COMPILER_ID_FILE}")

#-------------------------------------------------------------------------------
# Set the C++ compiler name and compiler version
#-------------------------------------------------------------------------------
SET(CFS_CXX_COMPILER_NAME ${CXX_ID})
SET(CFS_CXX_COMPILER_VER "${CXX_VERSION}")
SET(CFS_CXX_COMPILER_GNU_VER "${CXX_GCC_VERSION}")

#-------------------------------------------------------------------------------
# Now let's find out info about Fortran compiler.
#-------------------------------------------------------------------------------
SET(IDENTIFY_COMPILER_SRC "IdentifyFortranCompiler.F90")
SET(ID_FORTRAN "${CFS_BINARY_DIR}/share/scripts/identify_fortran.cmake")
CONFIGURE_FILE("${IDCOMP_TEMPL}" "${ID_FORTRAN}" @ONLY)

EXECUTE_PROCESS(
  COMMAND "${CMAKE_COMMAND}" -P "${ID_FORTRAN}"
  WORKING_DIRECTORY "${CFS_BINARY_DIR}/tmp"
  RESULT_VARIABLE RETVAL
  )

INCLUDE("${COMPILER_ID_FILE}")

#-------------------------------------------------------------------------------
# Set the Fortran compiler name and compiler version
#-------------------------------------------------------------------------------
SET(CFS_FORTRAN_COMPILER_NAME ${FC_ID})
SET(CFS_FORTRAN_COMPILER_VER "${FC_VERSION}")

#-------------------------------------------------------------------------------
# Check if compiler supports OpenMP
#-------------------------------------------------------------------------------
find_package(OpenMP)

if(USE_OPENMP)
  if(OPENMP_FOUND)
    set(CFS_C_FLAGS "${OpenMP_C_FLAGS}")
    set(CFS_CXX_FLAGS "${OpenMP_CXX_FLAGS} ${CFS_CXX_FLAGS}")
    if(APPLE)
      # let's hope this does not make the compiler use system gmp, hdf5, ...
      include_directories(AFTER SYSTEM "/usr/local/include")
      set(CFS_LINKER_FLAGS "${CFS_LINKER_FLAGS} -lomp -L/usr/local/lib")
    endif() # APPLE
  else()
    message(FATAL_ERROR "USE_OPENMP enabled but OpenMP not found on the system")
  endif() # OPENMP_FOUND
endif() # USE_OPENMP

#-------------------------------------------------------------------------------
# Check if we are using the GNU C++ or clang compiler
#-------------------------------------------------------------------------------
IF(CFS_CXX_COMPILER_NAME STREQUAL "GCC" OR CFS_CXX_COMPILER_NAME STREQUAL "CLANG")

  # MESSAGE("We are using the GNU C++ compiler. ${CMAKE_CXX_COMPILER}")
  # Obtain major version number of GCC or Clang
  STRING(REPLACE "." ";" CFS_CXX_COMPILER_VER_LIST ${CFS_CXX_COMPILER_VER})
  LIST(GET CFS_CXX_COMPILER_VER_LIST 0 CFS_CXX_COMPILER_MAJOR_VER)

  # we assue C++14 for CFS for any compiler (including icc below)
  set(CFS_CXX_FLAGS "-std=c++14 -Wuninitialized -Wno-error=unused-variable -Wno-error=maybe-uninitialized -DBOOST_NO_AUTO_PTR ${CFS_CXX_FLAGS}")
  set(CFS_C_FLAGS "-std=c11")

  IF(CFS_FSANITIZE)
    SET(CFS_CXX_FLAGS " -fsanitize=address ${CFS_CXX_FLAGS}")
    SET(CFS_C_FLAGS " -fsanitize=address ${CFS_C_FLAGS}")
  ENDIF()
  
  #-----------------------------------------------------------------------------
  # Determine compiler/linker flags according to build type
  #-----------------------------------------------------------------------------
  IF(DEBUG)
    SET(CFS_C_FLAGS "-Wall -fmessage-length=0 ${CFS_C_FLAGS}")
    # -Wold-style-cast Warnings about old C style casts. Since external libraries
    # make extensive use of it, we switch it off. To filter out the warnings in our own
    # code a command line like the following might be used
    # fgrep 'warning: use of old-style cast' out.txt | grep CFS_SOURCE_DIR | sort -u > old-style-cast.txt
    #
    SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -Wall -ftemplate-depth-100")

    # -frounding-math: is needed for CGAL library
    IF(USE_CGAL)
      SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -frounding-math")
    ENDIF(USE_CGAL)

    SET(CHECK_MEM_ALLOC 1)
  ELSE()
    # release!
    SET(CFS_C_FLAGS "-Wall -fmessage-length=0 ${CFS_C_FLAGS}")
    SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -Wall -ftemplate-depth-100")

    # the CFS_OPT_FLAGS are also used in FindCFSDEPS.cmake for CFSDEPS_*_FLAGS
    if(CFS_NATIVE)
      # -m64 -> 32 bit int and 64 bit pointers and long
      # further candidates: https://developer.amd.com/wordpress/media/2020/04/Compiler%20Options%20Quick%20Ref%20Guide%20for%20AMD%20EPYC%207xx2%20Series%20Processors.pdf
      # -march=native - has up to 20% boost against nonsense k8 and slight different results on some tests
      # -Ofast Maximize performance - almost no additional effect
      # -funroll-all-loops Enable unrolling - almost no additional effect
      # -flto Link time optimization - extremely slow linking on gcc with almost no effect
      # -param prefetch-latency=300 - Generate memory preload in structions - negative effect
      # some more for AMD Clang in the link above
      set(CFS_OPT_FLAGS "-m64 -march=native -Ofast ") 

    else()
      # AMD K8 is a years old legacy that worked, who know's a better portable id?
      set(CFS_OPT_FLAGS "-m64 -march=k8")
    endif()

  ENDIF() # end debug/release

  # Disable some annoying warnings.
  # note we have at least gcc 4.8
  # -Wno-overflow because of /boost/iostreams/filter/gzip.hpp:674:13: error: overflow in implicit constant conversion [-Werror=overflow]
  # -Wno-parentheses because of boost 1.67: warning: unnecessary parentheses in declaration of 'assert_not_arg' [-Wparentheses]
  SET(CFS_SUPPRESSIONS "-Wno-long-long -Wno-unknown-pragmas -Wno-comment -Wno-parentheses -Wno-unused-function ")
  # stuff that was removed with boost 1.67: -Wno-strict-aliasing -Wno-deprecated -Wno-attributes -Wno-unused-local-typedefs -Wno-overflow

  IF(CFS_CXX_COMPILER_NAME STREQUAL "GCC" AND CFS_CXX_COMPILER_VER VERSION_GREATER "5.0") # there is no >= and also there is no 5.0.0.0
    # /home/fwein/code/trunk/cfs/debug/include/boost/archive/detail/iserializer.hpp:65:1: error: this use of "defined" may not be portable [-Werror=expansion-to-defined]
     #if ! DONT_USE_HAS_NEW_OPERATOR

    SET(CFS_SUPPRESSIONS "${CFS_SUPPRESSIONS} -Wno-address -Wno-error=address -Wno-expansion-to-defined ")
  ENDIF()

  IF(CFS_CXX_COMPILER_NAME STREQUAL "GCC" AND CFS_CXX_COMPILER_VER VERSION_GREATER "6.0")
    # -Wno-misleading-indentation -Wno-error=placement-new are for gcc 6.1.1 and boost 1.61 maybe remove when a newer boost ist available!
    # however cfsbin has linkin problems with boost 1.61 hence we have also -Wno-address for boost 1.58
    # we must not set this to CFS_SUPRESSIONS because these also become CMAKE_C_FLAGS and then the following happens:
    # CheckFortranRuntime.cmake (CFS) ->  FortranCInterface.cmake (system) -> Detect.cmake (system) -> try_compile(FortranCInterface_COMPILED
    # this calls a C test (cfs/BUILD/CMakeFiles/FortranCInterface -> make) and reports "command line option *** is valid for C++ but not for C"
    # for debug with -Werror this fails and as a result Fortran name mangling does not work (BUILD/include/def_cfs_fortran_interface.hh is empty)
    SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -Wno-misleading-indentation -Wno-placement-new")
  ENDIF()

  if(CFS_CXX_COMPILER_NAME STREQUAL "GCC" AND CFS_CXX_COMPILER_VER VERSION_GREATER "7.0")
    # on macOS with gcc-7.1 
    # /include/boost/archive/detail/iserializer.hpp:208:9: error: this use of "defined" may not be portable  #if DONT_USE_HAS_NEW_OPERATOR
    set(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -Wno-expansion-to-defined ")
  endif()

  if(CFS_CXX_COMPILER_NAME STREQUAL "GCC" AND CFS_CXX_COMPILER_VER VERSION_GREATER "8.0")
    set(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -Wno-stringop-truncation ")
  endif()


  # most specific -Wno-error= are for plain old boost and gcc >= 6. Check to skip them for newer boost than 1.58
  IF(CFS_CXX_COMPILER_NAME STREQUAL "CLANG")
    # required for boost:  error: unused typedef 'boost_static_assert_typedef_890
    # also boost: /include/boost/bimap/support/iterator_type_by.hpp:128:1: error: class member cannot be redeclared
    # ResultHandler.cc: error: expression with side effects will be evaluated despite being used as an operand to 'typeid' "if( typeid(*fct) == typeid(FieldCoefFunctor<Double>"
    # include/boost/smart_ptr/detail/sp_counted_base_clang.hpp: warning: '_Atomic' is a C11 extension
    SET(CFS_SUPPRESSIONS "${CFS_SUPPRESSIONS} -Wno-overloaded-virtual -Wno-redeclared-class-member -Wno-potentially-evaluated-expression -Wno-c11-extensions")
    # -Wno-constant-conversion: boost/iostreams/filter/gzip.hpp:674:16: error: implicit conversion from 'const int' to 'char' changes value from 139 to -117
    set(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -Wno-constant-conversion")
    # include/muParserBytecode.h:51:7: error: anonymous types declared in an anonymous union are an extension
    set(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -Wno-nested-anon-types")
    # not all gcc options are compatible with clang (mac)
    set(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} -Wno-unknown-warning-option")
  ENDIF()

  IF(APPLE)
    # Check wether the compiler has the -sysroot= flag.
    SET(CXX_HAS_SYSROOT_FLAG_SOURCE "
int
main ()
{
  return 0;
}
")
    SET(CMAKE_REQUIRED_FLAGS "-sysroot=${CMAKE_OSX_SYSROOT}")
    CHECK_CXX_SOURCE_COMPILES("${CXX_HAS_SYSROOT_FLAG_SOURCE}" CXX_HAS_SYSROOT_FLAG)
    UNSET(CMAKE_REQUIRED_FLAGS)
  ENDIF()

  # see https://en.wikipedia.org/wiki/Gcov
  IF(CFS_COVERAGE)
    SET(CFS_C_FLAGS "-fprofile-arcs -ftest-coverage ${CFS_C_FLAGS}")
    SET(CFS_CXX_FLAGS "-fprofile-arcs -ftest-coverage ${CFS_CXX_FLAGS}")
    SET(CFS_LINKER_FLAGS "-fprofile-arcs -ftest-coverage ${CFS_LINKER_FLAGS}")
  ENDIF()

  IF(NOT USE_OPENMP)
    IF(NOT USE_PHIST_EV OR USE_PHIST_CG)
      SET(CFS_C_FLAGS "-Werror ${CFS_C_FLAGS}")
      SET(CFS_CXX_FLAGS "-Werror ${CFS_CXX_FLAGS}")
    ENDIF()
  ENDIF()


  IF(USE_CGAL) # does CGAL really use C?
    SET(CFS_C_FLAGS "-frounding-math ${CFS_C_FLAGS}")
    SET(CFS_CXX_FLAGS "-frounding-math ${CFS_CXX_FLAGS}")
  ENDIF()
# end gcc/clang section
ELSEIF(CFS_CXX_COMPILER_NAME STREQUAL "MSVC")
  #-------------------------------------------------------------------------------
  # Check for Visual Studio C++ compiler
  #-------------------------------------------------------------------------------
#  include(CMakeDetermineVSServicePack)
#  DetermineVSServicePack( CFS_MSVC_SERVICE_PACK )
#  string(TOUPPER "${CFS_MSVC_SERVICE_PACK}" CFS_MSVC_SERVICE_PACK)
#  SET(CFS_MSVC_SERVICE_PACK "MS${CFS_MSVC_SERVICE_PACK}")

  # Disable some warnings. For details google for 'MSDN C/C++ Build Errors'.

  # For details google for 'MSDN Checked Iterators', '_SCL_SECURE_NO_WARNINGS'
  # and 'MSDN Security Enhancements in the CRT', _CRT_SECURE_NO_WARNINGS
  SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} /wd4996")

  # 'identifier' : macro redefinition
  # The macro identifier is defined twice. The compiler uses the second
  # macro definition.
  SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} /wd4005")

  # '%$S': virtual function overrides '%$pS', previous versions of the
  # compiler did not override when parameters only differed by
  # const/volatile qualifiers
  SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} /wd4373")

  # this is for WINDOWS 10
  SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} /D_WIN32_WINNT=0x0A00")

  # support for alternative tokens requires the following
  SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} /permissive- /Zc:twoPhase-")

#  MESSAGE(FATAL_ERROR "CFS_CXX_COMPILER_NAME ${CFS_CXX_COMPILER_NAME} Compiler")

#  MESSAGE(FATAL_ERROR "MSVC Compiler")
#-------------------------------------------------------------------------------
# Check for Intel C++ compiler
#-------------------------------------------------------------------------------
ELSEIF(CFS_CXX_COMPILER_NAME STREQUAL "ICC")
  #-----------------------------------------------------------------------------
  # Determine compiler/linker flags according to build type
  #-----------------------------------------------------------------------------
  IF(UNIX)
    IF(DEBUG)
      SET(CFS_C_FLAGS "-g -c99 -w1 -Wcheck -Werror ${CFS_C_FLAGS}")
      SET(CFS_CXX_FLAGS "-std=c++14 -g -w1 -Wcheck -Werror ${CFS_CXX_FLAGS}")
      SET(CFSDEPS_CXX_FLAGS "-std=c++14 -g ${CFSDEPS_CXX_FLAGS}") # TODO move this to FindCFSDEPS.cmake
      SET(CHECK_MEM_ALLOC 1)
    ELSE()
      # release case
      SET(CFS_C_FLAGS "-c99 -w0 -Werror ${CFS_C_FLAGS}")
      SET(CFS_CXX_FLAGS "-std=c++14 -w0 -Werror ${CFS_CXX_FLAGS}")
      SET(CFSDEPS_CXX_FLAGS "-std=c++14 -w0 ${CFSDEPS_CXX_FLAGS}")
      SET(CFS_SUPPRESSIONS "-wd1125,654,980 -Wno-unknown-pragmas -Wno-comment")
    ENDIF()
  ELSE()
    # this is for WINDOWS 10
    SET(CFS_C_FLAGS "${CFS_C_FLAGS} /D_WIN32_WINNT=0x0A00 /Qstd=c99")
    SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} /D_WIN32_WINNT=0x0A00 /Qstd=c++14")
    SET(CFSDEPS_CXX_FLAGS "${CFSDEPS_CXX_FLAGS} /D_WIN32_WINNT=0x0A00 /Qstd=c++14")
    IF(DEBUG)
      SET(CFS_C_FLAGS "/Z7 /w1 /Wcheck /Werror ${CFS_C_FLAGS}")
      SET(CFS_CXX_FLAGS "/Z7 /W1 /Wcheck /Werror ${CFS_CXX_FLAGS}")
      SET(CFSDEPS_CXX_FLAGS "/Z7 ${CFSDEPS_CXX_FLAGS}")
      SET(CHECK_MEM_ALLOC 1)
    ELSE()
      # release case
      SET(CFS_CXX_FLAGS "/W0 ${CFS_CXX_FLAGS}")
      SET(CFSDEPS_CXX_FLAGS "/W0 ${CFSDEPS_CXX_FLAGS}")
      SET(CFS_SUPPRESSIONS "/Qdiag-disable1125,654,980")
    ENDIF()
  ENDIF()

  # on windows deactivate all MS special features
  # should be an easy way to activate use of alternative tokens for intel
  # unfortunaley, this leads to many, many, many, issues using boost
  # --> deactivated......
  #  IF(WIN32)
	  #    SET(CFS_C_FLAGS "${CFS_C_FLAGS} /Za -wd2358")
	  #  SET(CFS_CXX_FLAGS "${CFS_CXX_FLAGS} /Za -wd2358")
	  #    SET(CFSDEPS_CXX_FLAGS "${CFSDEPS_CXX_FLAGS} /Za -wd2358")
  #  ENDIF()

  #---------------------------------------------------------------------------
  # Disable warnings about hidden overriden functions of base classes,
  # unknown pragmas (openmp, etc.) and multiline comments.
  # Additionally the following warnings / remarks are suppressed:
  #    191: type qualifier is meaningless on cast type
  #    279: controlling expression is constant
  #    654: overloaded virtual function
  #   1125: entity-kind "entity" is hidden by "entity" -- virtual function
  #         override intended?
  #   1170: invalid redeclaration of nested class
  #   2259: non-pointer conversion from "type" to "type" may lose significant bits
  #---------------------------------------------------------------------------
  IF(UNIX)
    SET(CFS_SUPPRESSIONS "-wd191,279,654,1125,1170,2259")
    SET(CFS_SUPPRESSIONS "${CFS_SUPPRESSIONS} -Wno-unknown-pragmas")
    SET(CFS_SUPPRESSIONS "${CFS_SUPPRESSIONS} -Wno-comment")
  ENDIF()

  #---------------------------------------------------------------------------
  # The  following flags and  defines are  necessary due  to incompatibilities
  # with  some versions  of the  GCC runtime  environment. The  problems often
  # occur in external libraries, most notably Boost. The favored policy should
  # be,  to fix  those problems  locally  by patching  the external  libraries
  # instead of  introducing global  flags and defines  for CFS++,  which might
  # break other stuff.
  #---------------------------------------------------------------------------
  IF(CFS_CXX_COMPILER_VER MATCHES "11\\.")
    SET(CFS_SUPPRESSIONS "${CFS_SUPPRESSIONS} -fno-builtin-std::basic_istream::get")
    SET(CFS_SUPPRESSIONS "${CFS_SUPPRESSIONS} -fno-builtin-std::max")
  ENDIF(CFS_CXX_COMPILER_VER MATCHES "11\\.")
  # Open64 compiler support removed. See svn version 15997
ENDIF() # close all CXX compiler specific blocks

# common for all compilers
# adds debug information to the code such that vtune, valgrind, ... can show the lines of the hotspots
# this is different from adding gprof support by -pg wich adds changes the code to generate an output file
IF(CFS_PROFILING)
 SET(CFS_PROF_FLAGS "-g -fno-omit-frame-pointer")
ENDIF()



#-------------------------------------------------------------------------------
# Check for Intel Fortran compiler
#-------------------------------------------------------------------------------
IF(CFS_FORTRAN_COMPILER_NAME STREQUAL "IFORT")
  #-----------------------------------------------------------------------------
  # Set Intel Fortran library paths in dedicated variables
  #-----------------------------------------------------------------------------
  STRING(REGEX REPLACE "bin/ifort" "lib" IFORT_LIB_PATH "${CMAKE_Fortran_COMPILER}")

  #-----------------------------------------------------------------------------
  # Paths for libraries and compilers are different for version 11 compilers.
  # There is even a difference between Linux and Mac OS X! Thank you Intel!!!
  #-----------------------------------------------------------------------------
  IF(CFS_FORTRAN_COMPILER_VER MATCHES "11\\." AND NOT CFS_DISTRO STREQUAL "MACOSX")
    STRING(REGEX REPLACE "bin/(.*)/ifort" "lib/\\1" IFORT_LIB_PATH "${CMAKE_Fortran_COMPILER}")
  ENDIF()

  LINK_DIRECTORIES(${IFORT_LIB_PATH})

#  SET(CFS_FORTRAN_DYNRT_LIBS "ifcoremt;imf;dl")
#  IF(NOT CFS_ARCH STREQUAL "IA64")
#    SET(CFS_FORTRAN_DYNRT_LIBS
#      "svml"
#      ${CFS_FORTRAN_DYNRT_LIBS})
#  ENDIF(NOT CFS_ARCH STREQUAL "IA64")
#  SET(CFS_FORTRAN_STATRT_LIBS
#    "ifcoremt_pic"
#    "irc"
#    )

  IF(WIN32)
    #-----------------------------------------------------------------------------
    # Copy compiler runtime .dlls to bin directory
    #-----------------------------------------------------------------------------
    SET(INTEL_DLLS
        libifcoremd.dll
        libifportmd.dll
        libiomp5md.dll
        libmmd.dll
        svml_dispmd.dll
    )
    IF(DEBUG)
      SET(INTEL_DLLS 
        ${INTEL_DLLS}
        libifcoremdd.dll
        libmmdd.dll
      )
    ENDIF()
    
    SET(LIB_DEST_DIR "${CFS_BINARY_DIR}/bin/")
    GET_FILENAME_COMPONENT(INTEL_COMPILER_DIR ${CMAKE_Fortran_COMPILER} PATH)    

    IF(CFS_FORTRAN_COMPILER_VER MATCHES "20\\.")
      # intel oneApi
      SET(ICC_REDIST_DIR "${INTEL_COMPILER_DIR}/../../redist/intel64_win/compiler/")
    ELSE()
      #intel parallel studio pre oneApi
      SET(ICC_REDIST_DIR "${INTEL_COMPILER_DIR}/../../redist/intel64/compiler/")
    ENDIF()
    
    MESSAGE(STATUS "Copying INTEL redistributable files from ${ICC_REDIST_DIR} to ${LIB_DEST_DIR}")
    FOREACH(lib IN LISTS INTEL_DLLS)
      FILE(COPY ${ICC_REDIST_DIR}/${lib} DESTINATION ${LIB_DEST_DIR})
    ENDFOREACH()
  ENDIF(WIN32)

ENDIF(CFS_FORTRAN_COMPILER_NAME STREQUAL "IFORT")


#-------------------------------------------------------------------------------
# Check if we have a valid combination of C++ and Fortran compilers.
#-------------------------------------------------------------------------------
IF(CFS_CXX_COMPILER_NAME STREQUAL "" OR  CFS_FORTRAN_COMPILER_NAME STREQUAL "")
  MESSAGE(SEND_ERROR "Combination of C++ and Fortran compilers not supported!")
ENDIF()

#-------------------------------------------------------------------------------
# Set compiler/linker flags for all build types
#-------------------------------------------------------------------------------
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${CFS_C_FLAGS} ${CFS_PROF_FLAGS} ${CFS_OPT_FLAGS} ${CFS_SUPPRESSIONS}")
STRING(STRIP "${CMAKE_C_FLAGS}" CMAKE_C_FLAGS)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CFS_CXX_FLAGS} ${CFS_PROF_FLAGS} ${CFS_OPT_FLAGS} ${CFS_SUPPRESSIONS}")
STRING(STRIP "${CMAKE_CXX_FLAGS}" CMAKE_CXX_FLAGS)
SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${CFS_PROF_FLAGS} ${CFS_LINKER_FLAGS}")
STRING(STRIP "${CMAKE_EXE_LINKER_FLAGS}" CMAKE_EXE_LINKER_FLAGS)
SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} ${CFS_PROF_FLAGS} ${CFS_LINKER_FLAGS}")
STRING(STRIP "${CMAKE_MODULE_LINKER_FLAGS}" CMAKE_MODULE_LINKER_FLAGS)
SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${CFS_PROF_FLAGS} ${CFS_LINKER_FLAGS}")
STRING(STRIP "${CMAKE_SHARED_LINKER_FLAGS}" CMAKE_SHARED_LINKER_FLAGS)

# MESSAGE("C++ name: ${CFS_CXX_COMPILER_NAME}")
# MESSAGE("C++ version: ${CFS_CXX_COMPILER_VER}")
# MESSAGE("FORTRAN Compiler ${CFS_FORTRAN_COMPILER_NAME}")
# MESSAGE("FORTRAN Compiler version ${CFS_FORTRAN_COMPILER_VER}")
# MESSAGE("CMAKE BUILD TYPE: ${CMAKE_BUILD_TYPE}")
# MESSAGE("CFS_ARCH_STR: ${CFS_ARCH_STR}")

