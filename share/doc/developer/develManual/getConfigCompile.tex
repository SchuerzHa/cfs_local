\chapter{Get, Configure and Compile CFS++}
This  section provides  a  description  for new  CFS++  developer  to get  the
necessary environment running  for doing CFS++ development. We  start with the
required  software packages  from the  operating system  software distribution
services  and give  the  required  commands to  install  them.  After that  we
describe how  to get the  CFS++ sources from  the development server.   In the
next section we describe how the source tree has to be configured for building
binaries.  We also mention how to  generate projects for Eclipse. An important
point while  doing FEM  development is pre-  and postprocessing.  We therefore
also describe how to obtain the pre- and postprocessors which are supported by
CFS++.  In the last section we give  hints on what other chapters and sections
are a good further reading for users with different interests.

In order  to get started with  CFS++ development it  is a good idea,  to learn
about  the   usage  of   CFS++  first  before   going  into  the   details  of
development. The  document which you  should therefore consider first,  is the
CFS++ user's tutorial \cite{Simkovics2010}. If you are comfortable using CFS++
the next  question to ask is,  wether you want to  setup up a  new machine for
doing  development, or  if  you are  going  to use  one  of the  preconfigured
machines in Erlangen or Klagenfurt.  In  the former case you should start with
sections  \ref{sec:list_of_ext_packages}  and \ref{sec:obtaining_prepostprocs}
before following the  steps outlined in the remaining  sections of the current
chapter.    In  Sec.    \ref{sec:first_time_implementing}  a   somewhat  rough
description is given about the  structure of CFS++ by explaining the necessary
code parts for  a pseudo PDE.  To get hands-on experience  with the code, Sec.
\ref{sec:code_in_action}  describes how  to examine  the static  and transient
behavior of the code by recording callgraphs and by using a debugger.

\section{Setting up a Machine for CFS++ Development}
\label{sec:list_of_ext_packages}

\subsection{UNIX-based Operating Systems}

The list of  required packages depends on the  operating system and especially
for Linux based OSes on the distribution.  We provide a bootstrap shell script
for installing the required packages  from the command line.  The script works
for some of the  OSes most often in use by the  CFS++ developers. It is called
{\tt  bootstrap\_devel\_machine.txt}  and  can  be  obtained  from  the  CFS++
development server.   The shell  script needs to  be executed with  super user
privileges (consult  your computer's administrator!).   The following sequence
of commands is required for the setup process:

\input{pygmentized/qs_bootstrap.tex}
\attachfile[icon=PushPin,description={Shell   script   for   bootstrapping   a
development machine.},mimetype=text/plain]{attachments/qs_bootstrap.txt}

If  you use a  different distribution  you should  be able  to figure  out the
required  packages  by  examining  {\tt  bootstrap\_devel\_machine.txt}  quite
easily.

The  file  {\tt  bootstrap\_devel\_machine.txt}   contained  in  this  PDF  is
automatically   generated   by   concatenating  {\tt   distro.sh}   and   {\tt
  bootstrap\_devel\_machine.sh} from  the CFS++ source  code.  If you  want to
implement support for a new OS, you should therefore follow the examples given
there and commit your changes back to the repository.

\subsubsection{Linux}

The  description given in  the section  above applies  to all  supported Linux
distributions. These include:

\begin{description}

\item[openSUSE and  SLE] All openSUSE,  SLES and  SLED versions from  the 11.x
  series should work  out of the box.   If some package is  not available from
  the standard  repositories, there is  a good chance  to find it  at openSUSE
  software  search  \url{http://software.opensuse.org/search}, openSUSE  build
  service  \url{https://build.opensuse.org}  or  one   of  the  other  package
  repositories like  PackMan (\url{http://packman.links2linux.org}).  A mirror
  of     SLES/SLED    10     and     11    DVDs     can     be    found     at
  \url{http://demeter.uni-regensburg.de/}.

\noindent Checking which package provides an already installed file:
\begin{verbatim}
rpm -q --whatprovides /usr/bin/g++
\end{verbatim}

\item[Debian Based] Ubuntu versions from 10.04 LTS are known to work.  Earlier
  versions may  also work. Since  Ubuntu is  derived from Debian,  also recent
  Debian releases  should work.  Support for  Linux Mint, which  is in  turn a
  spinoff from Ubuntu, has also been incorporated.

\noindent Checking which package provides an already installed file:
\begin{verbatim}
dpkg -S /usr/bin/g++
\end{verbatim}

Ubuntu is a little  special among other Linux distros since it  uses Dash as a
shell instead of  Bash. Therefore some shell scripts written  for Bash may not
work on  Ubuntu. Go  to \url{https://wiki.ubuntu.com/DashAsBinSh} for  tips on
how  to  make your  scripts  POSIX  compliant.   We  also provide  the  script
\texttt{checkbashisms}  in  {\tt  share/scripts} which  gives  warnings  about
bash-specific  syntax. On  Ubuntu, there  exists also  the \texttt{devscripts}
package, which contains a lot of other tools for script development.

\item[Fedora] Fedora support has been tested on release 13.

\noindent Checking which package provides an already installed file:
\begin{verbatim}
rpm -q --whatprovides /usr/bin/g++
\end{verbatim}

\item[CentOS] Support  has been added  for CentOS  5.x - 7.x.  Therefore, also
  other RHEL clones (e.g. Scientific Linux) should work.
\end{description}%

\subsubsection{Mac OS X}

As Mac OS X is also a UNIX-based operating system, the steps described in Sec.
\ref{sec:list_of_ext_packages} also  guide the user through  the setup process
of a Mac  OS X development machine.   The bootstrap script was  tested on Snow
Leopard (10.6)  but should  also work  on Leopard (10.5)  and the  Lion (10.7)
release.

Inside  the  centos6  nightly  test  Vagrant box  a  Linux-to-Mac  OS  X  10.6
cross-compiler toolchain  has been  installed.  The  toolchain has  been built
using   a   CMake   script\footnote{CMake  build   script   for   Linux-to-OSX
  gcc/g++/gfortran                  cross-compiler                  toolchain:
  \url{http://www.cmake.org/pipermail/cmake/2013-September/055881.html}},
which can  build cross-compiler toolchains  for CentOS  6, SLES 11  and Ubuntu
12.04. The script and accompanying binary packages\footnote{OSX cross-compiler
  build script  and binary  packages: \url{\CFSDSSFTP/binaries/xcode}}  is also
available from the CFS++ development server.

The following list includes a number of required packages on Mac OS X. 
\begin{itemize}
\item XCode - \url{http://developer.apple.com/technologies/tools/xcode.html}
\item MacPorts - \url{http://www.macports.org} has latest GCC, TCL,  etc.
\item CMake - \url{http://www.cmake.org}
%\attachfile[icon=PushPin,description={List
%of                   packages                  installed                  from
%MacPorts.},mimetype=text/plain]{attachments/mac_ports_installed_pckgs.txt}
%cf. \url{http://pc.freeshell.org/comp/macbook}
\item   For   Fortran    go   to   \url{http://r.research.att.com/tools/}   or
  \url{http://www.macresearch.org/gfortran-leopard}.         Also        visit
  \url{http://hpc.sourceforge.net/}.
\item   Building    a   cross    compiler   for   Mac    OS   X    on   Linux:
  \url{http://devs.openttd.org/~truebrain/compile-farm/apple-darwin9.txt}. Further
  informations  about building  OSX cross-compilers  can be  found inside  the
  corresponding CMake script.
\end{itemize} 


\subsection{Windows (XP, Vista, 7)}

We support building of CFS++ for Windows in two different flavors:

\begin{description}
\item[Cross-compiling  from  Linux]  At  the   moment  we  support  two  MinGW
  toolchains  on  CentOS 6  (gcc  4.5.3)  and  Ubuntu  12.04 (gcc  4.6.3,  cf.
  Sec.  \ref{sec:mingw_linux_crosscompile}).    The  CentOS  6   toolchain  is
  installed inside the centos6 Vagrant box for nightly testing as a reference.
\item [Compiling on Windows] by making  use of the MinGW toolchain inside {\tt
  cmd.exe} or MSYS (minimal Bash system) environments with some additional GNU
  utilities. A  32-bit toolchain is  installed inside the winxp32  Vagrant box
  for nightly testing as a reference.
\end{description}%
%
Similar to UNIX systems, there exists an automated setup script for the native
tools on  Windows. The  script can  be downloaded  from the  CFS++ development
server  at  \url{\CFSDSFTP/scripts/bootstrap_devel_machine.cmd}   or  in  {\tt
  share/scripts} inside the  CFS++ source. The script and  all needed packages
are also mirrored at \url{\CFSDSSFTP/binaries/mingw/win}. The script should be
placed inside  a directory, where  the MinGW  environment is to  be installed,
e.g.  {\tt  E:{\textbackslash}mingw\_env} cf.   Fig. \ref{fig:mingw_env_xp32}.
The drive containing this folder should have about 12GB of free space.
 
\begin{figure}[htb] 
  \centering
  \begin{overpic}[width=0.98\textwidth]{pics/mingw_env_xp32}
  \end{overpic}
  \caption{Contents of the  MinGW environment folder for  a 32-bit environment
    on Windows XP.}
  \label{fig:mingw_env_xp32}
\end{figure}%
%
The  bootstrap  script  will  try  to  perform its  task  with  as  less  user
interaction as  possible. It  may however  be necessary  to define  some proxy
settings inside the script and add some exceptions to the Windows firewall for
{\tt ftp.exe} and {\tt  wget.exe}. Once finished, the user will  get a link to
Console2  on  his Desktop.  Inside  Console2  the different  environments  are
available as Fig. \ref{fig:mingw_env_console2} shows.

\begin{figure}[htb] 
  \centering
  \begin{overpic}[width=0.8\textwidth]{pics/mingw_env_console2}
  \end{overpic}
  \caption{CMD and MSYS environments can be started inside Console2.}
  \label{fig:mingw_env_console2}
\end{figure}%
%
CFS++ can be  built inside the CMD  or MSYS environments but in  some cases it
may  be necessary  to use  the MSYS  environments.  This  can be  the case  if
packages, which  contain an {\tt  autoconf/automake} build system, have  to be
built. Such packages  require a Bash shell interpreter for  building using the
{\tt configure/make/make  install} mechanism  known from  the UNIX  world.  If
e.g. CGAL  is switched on,  GMP and  MPFR are pulled  in, which need  the MSYS
environment.%
%
Finally we want to describe the most important packages that make up the MinGW
environment.
%
\begin{description}
\item[GNU Compilers]  There exist several distributions  for MinGW toolchains.
  The  toolchain  set  up  by  the  bootstrap  script  is  the  one  from  the
  MinGW-Builds\footnote{MinGW-builds                                  project:
    \url{http://www.sourceforge.net/projects/mingwbuilds}}  project.   It  has
  been chosen since  it contains dual target GNU compilers,  Python and Qt 4.8
  environments  built using  these compilers  are available.   The environment
  used   for  testing   and   inside   the  winxp32   Vagrant   box  was   the
  \emph{TDM-GCC}\footnote{TDM-GCC toolchain: \url{http://tdm-gcc.tdragon.net}}
  toolchain.   In  their download  section  one  can either  download,  single
  packages or a  bundle installer for either 32-bit  or 64/32-bit development.
  We chose the  tdm64-gcc bundle installer.  This package  provides a standard
  Windows  installer  with  all  required  packages for  C,  C++  and  Fortran
  compilers. Another toolchain is available at \url{http://www.equation.com}.

\item[GNU Tools]  We also need the  usual GNU tools, like  gmake, for building
  software. There are  also two options available for  obtaining these.  There
  exists  an   older  project  \emph{GNU  utilities   for  Win32}\footnote{GNU
    utilities   for  Win32:   \url{http://unxutils.sourceforge.net}}  or   the
  \emph{GnuWin32}\footnote{GnuWin32                                   project:
    \url{http://gnuwin32.sourceforge.net}}   project,   which  provides   more
  up-to-date versions  of the utilities.  After  following their corresponding
  download and installation instructions, one should end up with the installed
  trees.    One   has   to   add   the   corresponding   binary   dirs   ({\tt
    GNUWIN32\_INSTALL\_DIR/bin}        for       GnuWin32        or       {\tt
    UNXUTILS\_INSTALL\_DIR/usr/local/wbin}   for  UnxUtils)   to  the   system
  path. There exists a known problem, where {\tt patch.exe} has a problem with
  Windows 7 User Access Control UAC\footnote{Using ``patch'' from the GnuWin32
    project                   on                  Windows                   7:
    \url{http://math.nist.gov/oommf/software-patchsets/patch_on_Windows7.html}}. The
  {\tt patch.exe}  therefore needs to be  endowed with a manifest  first! When
  applying  a patch  manually  it is  usually  a  good idea  to  add the  {\tt
    --binary}  command  line  switch to  avoid  CR/LF  problems\footnote{Patch
    error:       Assertion       failed,       hunk,       file       patch.c:
    \url{http://solutionslog.blogspot.de/2009/07/patch-error-assertion-failed-hunk-file.html}}

\item[CMake] for Windows can be obtained from download section of the official
  homepage\footnote{CMake                    download                    page:
    \url{http://www.cmake.org/cmake/resources/software.html}}.

\item[Subversion]  Inside the  bootstrapped  environment we  use SlikSVN.  The
  best-known    Subversion   distribution    on    Windows    seems   to    be
  TortoiseSVN\footnote{Subversion                 for                 Windows:
    \url{http://tortoisesvn.tigris.org}}.   A Java-based  platform independent
  alternative    which     also    provides    command    line     tools    is
  SVNKit\footnote{SVNKit: \url{http://svnkit.com}}.

\item[Git] If the CFS++ working copy is Git-based, then one should get Git for
  Windows\footnote{Git  for  Windows: \url{http://msysgit.github.com}}.   This
  package also  provides a minimal  system (MSYS), which the  bootstrap script
  extends with  a few  extra packages  to get  a full-fledged  environment for
  building  automake-based software.   There seems  to be  also a  TortoiseGit
  project\footnote{TortoiseGit                                        project:
    \url{http://code.google.com/p/tortoisegit}}.

\item[Intel MKL] If the optimized solvers from  MKL need to be used, a Windows
  distribution (e.g. as  provided by the latest  Intel Compiler installations)
  is required. Unlike on UNIX-based  systems, no advanced system for automatic
  detection of the  required link parameters has been implemented  so far. The
  corresponding   settings    can   be   found   and    hand-tuned   in   {\tt
    cmake\_modules/FindIntelMKL.cmake},  however.  The  only  versions of  MKL
  known to work on  Windows at the moment are 10.0.5.025  for the 64-bit MinGW
  environments and 11.0 for the 32-bit environments. Be warned, that switching
  compilers most certainly brakes compatibility  with MKL due to the different
  OpenMP runtimes of the GNU and Microsoft compilers!
\end{description}%
%
The binary directories of the aforementioned distributions need to be added to
the system PATH,  so that the utilities  can be found afterwards  by the build
system.   The configuration  of CFS++  on Windows  is then  pretty similar  as
compared  to  UNIX. The  only  difference  is,  that  one should  specify  the
additional  parameter {\tt  -G ``MinGW  Makefiles''}  on the  command line  of
CMake,  since  Microsoft  NMake  files  or Visual  Studio  projects  would  be
generated  otherwise.   This is  however  not  necessary inside  the  Console2
environments since there a patched CMake  is used, whose default generator can
be configured using the {\tt CMAKE\_DEFAULT\_GENERATOR} environment variable.%
%
There  is also  only  very limited  support for  the  Microsoft Visual  Studio
compilers, since we require also a  Fortran compiler. Since a Fortran compiler
is not part of the Microsoft tools, either the Intel Fortran compiler would be
needed or the GNU Fortran compiler could be used.  This would however, require
some changes to  our build project\footnote{Fortran for  C/C++ developers made
  easier  with CMake:  \url{http://www.kitware.com/blog/home/post/231}}. These
changes may be worth the effort in  the future, since Visual Studio Express is
an excellent  environment for debugging and  is also available for  free since
the 2008 release.

\newpage
\section{Obtaining the Sources}

Once the software  needed for CFS++ development from  the distribution has
been installed,  the next  step is  to checkout the  source code  from the
Subversion  \cite{svn} repository. All  necessary steps  are given  in the
shell  script at  the end  of this  section. We  first create  a directory
structure for our  development resources. Next we obtain  the sources from
the server. Note,  that upon the first connection you  will have to accept
the server's  certificate permanently (p)  and decide, wether you  wish to
save your password locally.

\input{pygmentized/qs_obtain_sources.tex}
\attachfile[icon=PushPin,description={Shell script for obtaining CFS++ sources.},mimetype=text/plain]{attachments/qs_obtain_sources.txt}

Further information  about Subversion can be found  in Sec. \ref{sec:svn}.

A nice alternative, especially for working on a laptop, is the distributed
versioning system Git.  A basic howto for obtaining a  Git working copy of
the CFS++ sources, some basic commands and pointers to additional material
are given in Sec. \ref{sec:git}.

\section{Configuring the Build Tree and Building Binaries}

We use  the CMake build system  (cf.  Sec.  \ref{sec:cmake})  to configure our
source tree for building on a  wide range of platforms.  Unlike the well-known
configure/make/make install methodology from Unix-like systems, CMake supports
also other  platforms like Windows.   It generates build descriptions  for the
native  development environments (e.g.   Makefiles on  Unix and  Visual Studio
projects  on Windows)  from very  simple input  files in  CMake  syntax. Using
CMake,  it   is  also   possible  to  do   out-of-source  builds   (cf.   Fig.
\ref{fig:understand_src_bin_dir}), which means, that from a single source tree
multiple   build   trees   may   generated   (e.g.  for   debug   or   release
executables). This way,  CMake keeps the different builds  separate and avoids
cluttering the  source directory with a  huge number of  generated files.  The
latter behavior can  make the output of the  Subversion status commands almost
useless.  By  doing dependency  analysis  CMake  can  also generate  Makefiles
suitable for parallel builds.

\begin{figure}[htb] 
  \centering
  \begin{overpic}[width=0.8\textwidth]{pics/understand_src_bin_dir}
  \end{overpic}
  \caption{Two  build directories  with  different settings  generated from  a
single source directory.}
  \label{fig:understand_src_bin_dir}
\end{figure}%

In the following script we create  two empty build directories. In one of them
we will  build a debug executable,  whereas in the  other one we will  build a
release  executable.   Before configuring  we  set  our  desired compilers  by
creating  new  environment variables.   After  that  we  change to  the  build
directories and configure them.  For this  purpose we also have to specify the
location of CFSDEPS to CMake and where the dowloaded sources of libraries will
be  stored  (CFS\_DEPS\_CACHE\_DIR, cf.   Sec.   \ref{sec:cfsdeps}).  We  also
specify, that we want to use standard  Unix make files for building. As a last
step  we  have to  build  the  libraries, that  CFS++  depends  upon, and  the
executables.  This step  may take a considerable amount of  time for the first
time, but  will not  be necessary anymore,  if the  CFSDEPS do not  change any
more. Please  note, that you should always  specify the build of  cfsdeps in a
separate step before the build of CFS++, since race conditions during parallel
builds could occur at the moment.   If your home directory is a network share,
please  take  Sec.   \ref{sec:being_careful_with_shares} into  account  before
executing the following commands.

\input{pygmentized/qs_configure_build}

\subsection{Being Careful with Network Shares}
\label{sec:being_careful_with_shares}
\textcolor{red}{IMPORTANT  NOTE:  Avoid  putting  your  build  directories  on
network shares! User home directories tend  to be such shares.  CMake and make
require certain  file access mechanisms, that  may not be  available on shared
folders. Moreover,  the build will take much  longer and it may  not even make
sense to share executables  among different machine architectures or operating
system versions!}

You can check  whether your home directory is mounted from  a network share by
typing
\begin{verbatim}
mount | grep $HOME
\end{verbatim}
into a shell prompt. If you get a response like this
\begin{verbatim}
//server/user_homes/georg/ on /home/users/georg type cifs (rw)
\end{verbatim}
you should not put your build directory inside your home directory.

It is however perfectly sane to  put your source directory on a network share.
Such practice is even advisable, since changes need to be made only once for a
set   of   sources   shared   by   multiple   computers.    Refer   to   Chap.
\ref{chap:mounting_sshfs}.  for sharing  the source directories with computers
which only have SSH access to the machine containing the sources.

\section{Generating a Project for Eclipse}
\label{sec:eclipse_setup_project}

The only  difference from the  procedure for generating Makfiles  described in
the section  above, is  that we  need to specify  a different  generator while
configuring the build tree with CMake. We do this by saying:

\begin{verbatim}
cmake -DDEBUG:BOOL=ON \ 
      -DCFS_DEPS_CACHE_DIR:PATH=$DEVDIR/CFSDEPSCACHE \ 
      -G "Eclipse CDT4 - Unix Makefiles" 
      $DEVDIR/CFS_TRUNK
\end{verbatim}

Now we have generated the two  files {\tt .cproject} and {\tt .project} in the
directory {\tt  \$DEVDIR\-/build\-/debug} in addition to the  Unix make files.
In Eclipse (with the CDT extension) we have to import our new project by doing
the   following  steps:  File   $\rightarrow$  Import   $\rightarrow$  General
$\rightarrow$  Existing  Project  into  Workspace  $\rightarrow$  Select  Root
Directory {\tt \$DEVDIR\-/build\-/debug}.

In order for the Eclipse code indexer  to find all headers it may be necessary
to add some  system and MKL include paths and  preprocessor defines.  This can
be  done under  Project $\rightarrow$  Properties $\rightarrow$  C/C++ Include
Paths and Symbols (cf.  Fig. \ref{fig:eclipse_cpp_inc_paths} for an example on
Mac OS X).

\begin{figure}[htb] 
  \centering 
  \subfloat[Additional include paths.]{
    \label{fig:eclipse_cpp_inc_paths}
    \begin{overpic}[height=5cm]{pics/eclipse_cpp_inc_paths}
    \end{overpic}
    }
  \qquad
  \subfloat[Importing a coding style.]{
    \label{fig:eclipse_import_bsd_style}
    \begin{overpic}[height=5cm]{pics/eclipse_bsd_style}
    \end{overpic}
  } 
  \caption{Specifying additional include directories and coding style settings.}
  \label{fig:eclipse_code_style_inc}
\end{figure}

In order to  maintain a common coding style  among Eclipse developers everyone
should     import    the     style    informations     attached     to    this
PDF.\attachfile[icon=PushPin,description={Eclipse                        coding
style.},mimetype=text/plain]{attachments/eclipse-code-style.txt}. The settings
can be  imported into  Eclipse by going  to Project  $\rightarrow$ Preferences
$\rightarrow$ C/C++  General $\rightarrow$ Code  Style $\rightarrow$ Configure
Workspace             Settings...              $\rightarrow$            Import
(cf. Fig. \ref{fig:eclipse_import_bsd_style}).

Also the  settings for  character encoding and  further editor  setting should
remain consistent among the developers.  The necessary settings in Eclipse can
be made  by accessing  Window $\rightarrow$ Preferences  $\rightarrow$ General
$\rightarrow$ Editors and Workspace (c.f Fig. \ref{fig:eclipse_cfs_settings}).
\begin{figure}[htb] 
  \centering 
  \subfloat[Editor settings.]{
    \label{fig:eclipse_editor_settings}
    \begin{overpic}[height=5cm]{pics/eclipse_editor_settings}
%      \put(10,20){\textcolor{red}{\bf Test}}
    \end{overpic}
    }
  \qquad
  \subfloat[Workspace settings.]{
    \label{fig:eclipse_workspace_settings}
    \begin{overpic}[height=5cm]{pics/eclipse_workspace_settings}
    \end{overpic}
  } 
  \caption{Settings for CFS++ in Eclipse.}
  \label{fig:eclipse_cfs_settings}
\end{figure}

For   convenience  the   corresponding  settings   files  are   attached  here
\attachfile[icon=PushPin,description={Eclipse                            editor
settings.},mimetype=text/plain]{attachments/org.eclipse.ui.editors.txt}
\attachfile[icon=PushPin,description={Eclipse                         workspace
settings.},mimetype=text/plain]{attachments/org.eclipse.core.resources.txt}. The
have               to                copied               to               the
workspace/.metadata/.plugins/org.eclipse.core.runtime/.settings  directory and
must be renamed to \*.prefs.


\section{Pre-/Postprocessors}
\label{sec:obtaining_prepostprocs}

Since its  creation CFS++  has come to  support a growing  number of  pre- and
postprocessors.  The three  most important being GiD, Gmsh  and ANSYS Classic.
The modelling process with these tools is documented in \cite{Simkovics2010}.

For the broader  picture of the software landscape around  CFS++ the reader is
referred    to     the    Advanced    Pre-     and    Postprocessing    Manual
\cite{CFSAdvPrePostManual}, which  features information on  common and special
pre-  and  postprocessing tasks  in  recipe-like style  for  a  wide range  of
commercial and free pre- and postprocessors.

We have  extended ParaView with a  reader for our HDF5  files.  Since building
ParaView by hand is not even an easy task in its own right, we have provided a
CMake external  project for  this in  the CFSDEPS. The  build of  ParaView can
therefore  be switched  on  inside  the CFS++  CMake  configuration using  the
advanced boolean flag  {\tt BUILD\_PARAVIEW}. The inner workings  of the build
are   described  more   closely  in   \ref{sec:build_paraview}  and   in  {\tt
  cfsdeps/paraview/External\_ParaViewSuperbuild.cmake}.

\section{The CFS++  Development Server}

All necessary resources (e.g. Subversion  server, Trac, CDash, etc.) for doing
CFS++ development have been installed on  common a dedicated server machine at
Erlangen.  Also a web server is running there, which serves as a portal to the
different services.  It  can be reached at \url{\CFSDSHTTPS}.  In this section
we briefly describe the services offered by the server.

\begin{figure}[htb] 
  \centering
  \begin{overpic}[width=0.8\textwidth]{pics/cfs_devel_server}
  \end{overpic}
  \caption{Home page of CFS++ development server with links to different services.}
  \label{fig:cfs_devel_server}
\end{figure}%

Figure \ref{fig:cfs_devel_server} shows the home page of the server along with
the pages for the Trac system  (cf. Sec. \ref{sec:trac}) and the CDash nightly
build  dashboard  (cf.  Sec.   \ref{sec:cdash}).   Furthermore the  Subversion
repository  can be browsed  online using  the SVN  link.  Nightly  built CFS++
manuals, Doxygen  documentation and  other documents related  to CFS++  can be
accessed via the  Documentation link. Under File Management  the developer can
find  nightly  built and  other  pre-built  CFS++  executables. Also  GMV  and
ParaView  executables  for different  platforms  can  be  found there.   Users
registered as administrators  may change the server settings  by accessing the
Administration link.


