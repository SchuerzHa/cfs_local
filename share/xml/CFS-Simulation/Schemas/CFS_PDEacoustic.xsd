<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  targetNamespace="http://www.cfs++.org/simulation"
  xmlns="http://www.cfs++.org/simulation"
  xmlns:cfs="http://www.cfs++.org/simulation"
  elementFormDefault="qualified">

  <xsd:annotation>
    <xsd:documentation xml:lang="en">
      Coupled Field Solver project CFS++
      Schema for PDE description for an acoustics PDE
    </xsd:documentation>
  </xsd:annotation>


  <!-- ******************************************************************* -->
  <!--   Definition of element for acoustics PDEs -->
  <!-- ******************************************************************* -->
  <xsd:element name="acoustic" type="DT_PDEAcoustic" substitutionGroup="PDEBasic">
    <xsd:unique name="CS_AcousticRegion">
      <xsd:selector xpath="cfs:region"/>
      <xsd:field xpath="@name"/>
    </xsd:unique>
  </xsd:element>


  <!-- ******************************************************************* -->
  <!--   Definition of data type for acoustics PDEs -->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_PDEAcoustic">
    <xsd:complexContent>
      <xsd:extension base="DT_PDEBasic">
        <xsd:sequence>

          <!-- Regions the PDE lives on -->
          <xsd:element name="regionList" minOccurs="1" maxOccurs="1">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="region" minOccurs="1" maxOccurs="unbounded">
                  <xsd:complexType>
                    <xsd:attribute name="name" type="xsd:token" use="required"/>
                    <xsd:attribute name="polyId" type="xsd:string" use="optional" default="default"> </xsd:attribute>
                    <xsd:attribute name="integId" type="xsd:string" use="optional" default="default"
                      > </xsd:attribute>
                    <xsd:attribute name="nonLinId" type="xsd:token" use="optional" default=""/>
                    <xsd:attribute name="flowId" type="xsd:token" use="optional" default=""/>
                    <xsd:attribute name="temperatureId" type="xsd:token" use="optional" default=""/>
                    <xsd:attribute name="dampingId" type="xsd:token" use="optional" default=""/>
                    <xsd:attribute name="lambId" type="xsd:token" use="optional" default=""/>
                    <xsd:attribute name="initialFieldId" type="xsd:token" use="optional" default=""/>
                    <xsd:attribute name="initialFieldD1Id" type="xsd:token" use="optional" default=""/>
                    <xsd:attribute name="complexFluid" type="DT_CFSBool" use="optional" default="no"/>
                  </xsd:complexType>
                </xsd:element>
              </xsd:sequence>
            </xsd:complexType>
          </xsd:element>

          <!-- Non-conforming interfaces of the PDE -->
          <xsd:element name="ncInterfaceList" type="DT_NcInterfaceList" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
              <xsd:documentation>Defines the non-conforming interfaces</xsd:documentation>
            </xsd:annotation>
          </xsd:element>

          <!-- List defining nonlinear types -->
          <xsd:element name="nonLinList" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <xsd:element name="westervelt" type="DT_AcousticNonLinWestervelt"/>
                <xsd:element name="kuznetsov" type="DT_AcousticNonLinKuznetsov"/>
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>

          <!-- List defining damping types -->
          <xsd:element name="dampingList" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <xsd:element name="pml" type="DT_DampingPML">
                  <xsd:annotation>
                    <xsd:documentation>Allows region to be a Perfectly Matched Layer</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
                <xsd:element name="rayleigh" type="DT_AcousticDampingRayl">
                  <xsd:annotation>
                    <xsd:documentation>Defines Rayleigh damping (parameters have to be specified in material-xml-file)</xsd:documentation>
                  </xsd:annotation>
                </xsd:element>
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>


          <!-- Type of FlowData (optional) -->
          <!-- List defining flows -->
          <xsd:element name="flowList" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <xsd:element name="flow" type="DT_BcInhomVector"/>
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>

          <!-- Type of LambData (optional) -->
          <!-- List defining flows -->
          <xsd:element name="lambList" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <xsd:element name="lamb" type="DT_BcInhomVector"/>
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>

          <!-- Type of FlowData (optional) -->
          <!-- List defining flows -->
          <xsd:element name="temperatureList" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <xsd:element name="temperature" type="DT_BcInhomScalar"/>
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>

          <!-- Initial conditions (optional) -->
          <xsd:element name="initialValues" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <!-- Initial state of previous sequence step / external file -->
                <xsd:element name="initialState" type="DT_InitialState" minOccurs="0" maxOccurs="1"/>
                <!-- Initial state given by used -->
                <xsd:element name="initialField" type="DT_InitialField" minOccurs="0" maxOccurs="1"/>
                <xsd:element name="initialFieldD1" type="DT_InitialField" minOccurs="0" maxOccurs="1"/>
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>

          <!-- Boundary Conditions & Loads (optional) -->
          <xsd:element name="bcsAndLoads" minOccurs="0" maxOccurs="1">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <!-- Dirichlet Boundary Conditions -->
                <xsd:element name="soundSoft" type="DT_BcHomScalar"/>
                <xsd:element name="pressure" type="DT_BcInhomScalar"/>
                <xsd:element name="potential" type="DT_BcInhomScalar"/>

                <!-- RHS Load Values-->
                <xsd:element name="velocity" type="DT_BcInhomVector"/>
                <xsd:element name="normalVelocity" type="DT_BcInhomScalar"/>
                <xsd:element name="normalAcceleration" type="DT_BcInhomScalar"/>

                <!-- acoustic source density -->
                <xsd:element name="acouSourceDensity"  type="DT_BcInhomScalar"/>

                <!-- Special Boundary Conditions -->
                <xsd:element name="absorbingBCs" type="DT_AbsorbingBC"/>
                <xsd:element name="rhsValues" type="DT_BcInhomScalar" minOccurs="0" maxOccurs="1"/>
                <xsd:element name="rhsDensity" type="DT_BcInhomScalar" minOccurs="0" maxOccurs="1"/>
                <xsd:element name="rhsMassTimeDeriv" type="DT_BcInhomScalar" minOccurs="0" maxOccurs="1"/>
                <xsd:element name="rhsMassConvective" type="DT_BcInhomScalar" minOccurs="0" maxOccurs="1"/>
                <xsd:element name="impedance" type="DT_AcouImpedance"/>
                <xsd:element name="boundaryLayer" type="DT_AcouBoundaryLayer"/>
                <xsd:element name="periodic" type="DT_BcPeriodic"/>
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>

          <!-- Desired solution values (optional) -->
          <!-- Currently this is not supported, since it is difficult
          to define in the acoustics case since it depends on the
          boundary conditions -->
          <xsd:element minOccurs="0" name="impedanceList">
            <xsd:complexType>
              <xsd:choice maxOccurs="unbounded">
                <xsd:element maxOccurs="1" minOccurs="0" name="mpp" type="DT_Mpp"/>
                <xsd:element maxOccurs="1" minOccurs="0" name="interpolImpedance"
                  type="DT_InterpolImpedance"/>
                <xsd:element maxOccurs="1" minOccurs="0" name="fctImpedance" type="DT_FctImpedance"
                />
              </xsd:choice>
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="storeResults" type="DT_AcousticStoreResults" minOccurs="0"
            maxOccurs="1"/>

        </xsd:sequence>

        <!-- Type of acoustic formulation (optional) -->
        <xsd:attribute name="formulation" use="optional" default="acouPressure">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="acouPressure"/>
              <xsd:enumeration value="acouPressureSOSatLaplace"/>
              <xsd:enumeration value="acouPotential"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>

        <xsd:attribute name="timeStepAlpha" use="optional" default="0.0" type="xsd:double"/>

        <!-- updated Lagrangian formulation -->
        <xsd:attribute name="updatedLagrange" use="optional" default="no">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="no"/>
              <xsd:enumeration value="yes"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>

        <!-- Type of acoustic pde -->
        <xsd:attribute name="subType" use="optional" default="standard">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="standard"/>
              <xsd:enumeration value="combustionNoise"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>

        <!-- Type of acoustic flow formulation -->
        <xsd:attribute name="flowFormulation" use="optional" default="standard">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="standard"/>
              <xsd:enumeration value="withDivergence"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>

        <!-- Type of function space (Lagrange / Legendre) with
        optional order -->
        <xsd:attribute name="type" use="optional" default="lagrange">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="lagrange"/>
              <xsd:enumeration value="legendre"/>
              <xsd:enumeration value="spectral"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>
        <xsd:attribute name="order" use="optional" type="xsd:positiveInteger" default="1"/>

      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>


  <!-- ******************************************************************* -->
  <!--   Definition of layer damping -->
  <!-- ******************************************************************* -->

  <!-- Definition of layer damping type -->
  <xsd:complexType name="DT_AcousticDampingLayer">
    <xsd:complexContent>
      <xsd:extension base="DT_DampingBasic">
        <xsd:sequence>
          <!-- Type of PML -->
          <xsd:element name="type" minOccurs="0" maxOccurs="1">
            <xsd:simpleType>
              <xsd:restriction base="xsd:token">
                <xsd:enumeration value="linear"/>
                <xsd:enumeration value="quadDist"/>
                <xsd:enumeration value="inverseDist"/>
                <xsd:enumeration value="exponential"/>
              </xsd:restriction>
            </xsd:simpleType>
          </xsd:element>

          <xsd:element name="xM" type="xsd:double" minOccurs="1" maxOccurs="1"/>
          <xsd:element name="yM" type="xsd:double" minOccurs="1" maxOccurs="1"/>
          <xsd:element name="zM" type="xsd:double" minOccurs="1" maxOccurs="1"/>
          <xsd:element name="radiusStart" type="xsd:double" minOccurs="1" maxOccurs="1"/>
          <xsd:element name="radiusEnd" type="xsd:double" minOccurs="1" maxOccurs="1"/>
          <xsd:element name="dampFactor" type="xsd:double" minOccurs="0" maxOccurs="1"/>
          <xsd:element name="dampFactorMax" type="xsd:double" minOccurs="0" maxOccurs="1"/>
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>


  <!-- ******************************************************************* -->
  <!--   Definition of fractional damping -->
  <!-- ******************************************************************* -->

  <!-- Definition of pml damping type -->
  <xsd:complexType name="DT_AcousticDampingFrac">
    <xsd:complexContent>
      <xsd:extension base="DT_DampingBasic">
        <xsd:sequence>

          <!-- Type of fractional damping algorithm -->
          <xsd:element name="fracAlg" minOccurs="0" maxOccurs="1">
            <xsd:simpleType>
              <xsd:restriction base="xsd:token">
                <xsd:enumeration value="gl"/>
                <xsd:enumeration value="blank"/>
              </xsd:restriction>
            </xsd:simpleType>
          </xsd:element>

          <!-- Number of past time-steps for fractional damping -->
          <xsd:element name="fracMemory" type="xsd:positiveInteger" minOccurs="0" maxOccurs="1"/>

          <!-- Type of interpolation -->
          <xsd:element name="interpolation" minOccurs="0" maxOccurs="1">
            <xsd:simpleType>
              <xsd:restriction base="xsd:token">
                <xsd:enumeration value="no"/>
                <xsd:enumeration value="lin1pt"/>
              </xsd:restriction>
            </xsd:simpleType>
          </xsd:element>
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>




  <!-- ******************************************************************* -->
  <!--   Definition of rayleigh damping -->
  <!-- ******************************************************************* -->

  <!-- Definition of rayleigh damping type -->
  <xsd:complexType name="DT_AcousticDampingRayl">
    <xsd:complexContent>
      <xsd:extension base="DT_DampingBasic">
        <xsd:sequence>
          <xsd:element name="adjustDamping" type="DT_CFSBool" minOccurs="0" maxOccurs="1"/>
          <xsd:element name="freq" type="DT_NonNegFloat" minOccurs="0" maxOccurs="1"/>
          <xsd:element name="ratioDeltaF" type="DT_NonNegFloat" minOccurs="0" maxOccurs="1"/>
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>



  <!-- ******************************************************************* -->
  <!--   Definition of thermoViscous damping -->
  <!-- ******************************************************************* -->

  <!-- Definition of thermoviscous damping type -->
  <xsd:complexType name="DT_AcousticDampingThermo">
    <xsd:complexContent>
      <xsd:extension base="DT_DampingBasic"> </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of westervelt nonlienarity -->
  <!-- ******************************************************************* -->

  <!-- Definition of westervelt nonlinearity type -->
  <xsd:complexType name="DT_AcousticNonLinWestervelt">
    <xsd:complexContent>
      <xsd:extension base="DT_NonLinBasic"> </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of kuznetsov nonlienarity -->
  <!-- ******************************************************************* -->

  <!-- Definition of kunetsov nonlinearity type -->
  <xsd:complexType name="DT_AcousticNonLinKuznetsov">
    <xsd:complexContent>
      <xsd:extension base="DT_NonLinBasic"> </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>



  <!-- ******************************************************************* -->
  <!--   Definition of variableSOS_CN1 nonlienarity -->
  <!-- ******************************************************************* -->
  <!-- Definition of variable SOS CN1 nonlinearity type -->
  <xsd:complexType name="DT_AcousticNonVariableSOS_CN1">
    <xsd:complexContent>
      <xsd:extension base="DT_NonLinBasic"> </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of variableSOS_CN2 nonlienarity -->
  <!-- ******************************************************************* -->
  <!-- Definition of variable SOS CN2 nonlinearity type -->
  <xsd:complexType name="DT_AcousticNonVariableSOS_CN2">
    <xsd:complexContent>
      <xsd:extension base="DT_NonLinBasic"> </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <!-- ******************************************************************* -->
  <!--   Definition of variableSOS_CN2Mean nonlienarity -->
  <!-- ******************************************************************* -->
  <!-- Definition of variable SOS CN2 nonlinearity type -->
  <xsd:complexType name="DT_AcousticNonVariableSOS_CN2Mean">
    <xsd:complexContent>
      <xsd:extension base="DT_NonLinBasic"> </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of the acoustic unknown types -->
  <!-- ******************************************************************* -->
  <xsd:simpleType name="DT_AcousticUnknownType">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="acouPressure"/>
      <xsd:enumeration value="acouPotential"/>
      <xsd:enumeration value="acouPmlAuxScalar"/>
      <xsd:enumeration value="acouPmlAuxVec"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- ******************************************************************* -->
  <!--   Definition of the boundary condition types for acoustics -->
  <!-- ******************************************************************* -->

  <!-- Element type for homogeneous Dirichlet boundary conditions -->
  <!-- We derive it by extending the base type -->
  <xsd:complexType name="DT_AcousticHD">
    <xsd:complexContent>
      <xsd:extension base="DT_BCBasic">
        <xsd:attribute name="name" type="xsd:token" use="required"/>
        <xsd:attribute name="quantity" default="acouPressure" type="DT_AcousticUnknownType"/>
        <xsd:attribute name="dof">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value=""/>
              <xsd:enumeration value="psi"/>
              <xsd:enumeration value="phix"/>
              <xsd:enumeration value="phiy"/>
              <xsd:enumeration value="phiz"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- Element type for inhomogeneous Dirichlet boundary conditions -->
  <!-- We derive it by extending the homogeneous case -->
  <xsd:complexType name="DT_AcousticID">
    <xsd:complexContent>
      <xsd:extension base="DT_AcousticHD">
        <xsd:attribute name="value" type="xsd:token" use="required"/>
        <xsd:attribute name="phase" type="xsd:token" use="optional" default="0.0"/>
        <xsd:attribute name="fileName" type="xsd:token" use="optional" default="none"/>
        <xsd:attribute name="interpolType" type="xsd:token" use="optional" default="NNB"/>
        <xsd:attribute name="noNodes" type="xsd:token" use="optional" default="-1"/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- Element type for specifying inhomogeneous neumann conditions -->
  <!-- Identical to that of inhomogeneous Dirichlet case -->
  <xsd:complexType name="DT_AcousticIN">
    <xsd:complexContent>
      <xsd:restriction base="DT_AcousticID">
        <xsd:attribute name="quantity" type="DT_AcousticUnknownType" default="acouPotential"/>
      </xsd:restriction>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- Element type for specifying loads -->
  <!-- Identical to that of inhomogeneous Dirichlet case -->
  <xsd:complexType name="DT_AcousticLoad">
    <xsd:complexContent>
      <xsd:extension base="DT_AcousticID"/>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- Element type for specifying absorbing boundary conditions -->
  <!-- Identical to that of homogeneous Dirichlet case -->
  <xsd:complexType name="DT_AbsorbingBC">
    <xsd:complexContent>
      <xsd:extension base="DT_AcousticHD">
        <xsd:attribute name="volumeRegion" type="xsd:token" use="required"/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- Element type for specifying an acoustic impedance -->
  <xsd:complexType name="DT_AcouImpedance">
    <xsd:attribute name="name" type="xsd:token" use="required"/>
    <xsd:attribute name="volumeRegion" type="xsd:token" use="required">
      <xsd:annotation>
        <xsd:documentation>The volume next to the impedance surface. Needed to read the correct environment values from the material file which are needed for calculating the impedance.</xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
    <xsd:attribute default="0" name="isNormalised" type="xsd:boolean">
      <xsd:annotation>
        <xsd:documentation>Tells CFS if the impedance was normalised, so it knows it has to multiply density and speed of sound to it.</xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
    <xsd:attribute name="impedanceId" type="xsd:token" use="optional" default="id"/>
  </xsd:complexType>

  <!-- Element type for specifying an acoustic boundary layer -->
  <xsd:complexType name="DT_AcouBoundaryLayer">
    <xsd:attribute name="name" type="xsd:token" use="required"/>
    <xsd:attribute name="volumeRegion" type="xsd:token" use="required">
      <xsd:annotation>
        <xsd:documentation>The volume next to the impedance surface. Needed to read the correct environment values from the material file which are needed for calculating the speed of sound.</xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
    <xsd:attribute name="cp" type="xsd:token" use="required">
      <xsd:annotation>
        <xsd:documentation>Specific heat capacity as constant pressure</xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
    <xsd:attribute name="cv" type="xsd:token" use="required">
      <xsd:annotation>
        <xsd:documentation>Specific heat capacity as constant volume</xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
    <xsd:attribute name="nu" type="xsd:token" use="required">
      <xsd:annotation>
        <xsd:documentation>Kinematic viscosity</xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
    <xsd:attribute name="k" type="xsd:token" use="required">
      <xsd:annotation>
        <xsd:documentation>Thermal conductivity</xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
  </xsd:complexType>


  <!-- RHS values, sources for the acoustic inside the domain -->
  <xsd:complexType name="DT_AcousticRHS">
    <xsd:sequence>
      <xsd:choice maxOccurs="unbounded">

        <!-- regions to be selected for reading in rhs values -->
        <xsd:element name="region" minOccurs="1" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:all>
              <xsd:element name="factor" type="xsd:token" minOccurs="0" maxOccurs="1" default="1"/>
              <xsd:element name="interpolation" minOccurs="0" maxOccurs="1">
                <xsd:complexType>
                  <xsd:all>
                    <xsd:element name="srcRegions">
                      <xsd:complexType>
                        <xsd:attribute name="names" type="xsd:token" use="required"/>
                        <xsd:attribute name="inputId" type="xsd:token" use="required"/>
                        <xsd:attribute name="coordSysId" type="xsd:token" use="optional"
                          default="default"/>
                      </xsd:complexType>
                    </xsd:element>
                    <xsd:element name="xyPlane" minOccurs="0" maxOccurs="1">
                      <xsd:complexType>
                        <xsd:attribute name="z" type="xsd:double" use="required"/>
                        <xsd:attribute name="tol" type="DT_NonNegFloat" use="optional"
                          default="1.0e-10"/>
                      </xsd:complexType>
                    </xsd:element>
                    <xsd:element name="tolerances" minOccurs="0" maxOccurs="1">
                      <xsd:complexType>
                        <xsd:all>
                          <xsd:element name="global" minOccurs="0" maxOccurs="1">
                            <xsd:complexType>
                              <xsd:attribute name="start" type="DT_NonNegFloat" use="required"/>
                              <xsd:attribute name="end" type="DT_NonNegFloat" use="optional"/>
                              <xsd:attribute name="inc" type="DT_PosFloat" use="optional"/>
                            </xsd:complexType>
                          </xsd:element>
                          <xsd:element name="local" minOccurs="0" maxOccurs="1">
                            <xsd:complexType>
                              <xsd:attribute name="start" type="DT_NonNegFloat" use="required"/>
                              <xsd:attribute name="end" type="DT_NonNegFloat" use="optional"/>
                              <xsd:attribute name="inc" type="DT_PosFloat" use="optional"/>
                            </xsd:complexType>
                          </xsd:element>
                        </xsd:all>
                      </xsd:complexType>
                    </xsd:element>
                    <xsd:element name="restartFileMode" minOccurs="0" maxOccurs="1" default="rw">
                      <xsd:simpleType>
                        <xsd:restriction base="xsd:string">
                          <xsd:enumeration value="r"/>
                          <xsd:enumeration value="w"/>
                          <xsd:enumeration value="rw"/>
                        </xsd:restriction>
                      </xsd:simpleType>
                    </xsd:element>
                    <xsd:element name="nodeWarnings" minOccurs="0" maxOccurs="1">
                      <xsd:complexType>
                        <xsd:attribute name="display" use="required">
                          <xsd:simpleType>
                            <xsd:restriction base="xsd:string">
                              <xsd:enumeration value="yes"/>
                              <xsd:enumeration value="no"/>
                              <xsd:enumeration value="verbose"/>
                            </xsd:restriction>
                          </xsd:simpleType>
                        </xsd:attribute>
                        <xsd:attribute name="writeNodes" type="DT_CFSBool" use="optional"
                          default="no"/>
                      </xsd:complexType>
                    </xsd:element>
                  </xsd:all>
                  <xsd:attribute name="justInterpolate" type="DT_CFSBool" use="optional"
                    default="no"/>
                  <xsd:attribute name="overwriteOldSrcs" type="DT_CFSBool" use="optional"
                    default="yes"/>
                </xsd:complexType>
              </xsd:element>
              <xsd:element name="asyncSteps" minOccurs="0" maxOccurs="1" default="no">
                <xsd:simpleType>
                  <xsd:restriction base="xsd:string">
                    <xsd:enumeration value="no"/>
                    <xsd:enumeration value="nearest"/>
                    <xsd:enumeration value="interpolate"/>
                  </xsd:restriction>
                </xsd:simpleType>
              </xsd:element>
            </xsd:all>
            <xsd:attribute name="name" type="xsd:token" use="required"/>
            <xsd:attribute name="inputId" type="xsd:token" use="optional" default="default"/>
          </xsd:complexType>
        </xsd:element>
      </xsd:choice>
    </xsd:sequence>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of data type for damping model -->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_AcousticDamp">

    <!-- Type of damping -->
    <xsd:attribute name="type" use="optional" default="no">
      <xsd:simpleType>
        <xsd:restriction base="xsd:token">
          <xsd:enumeration value="no"/>
          <xsd:enumeration value="fractional"/>
          <xsd:enumeration value="rayleigh"/>
          <xsd:enumeration value="absorbingBC"/>
          <xsd:enumeration value="thermoViscous"/>
        </xsd:restriction>
      </xsd:simpleType>
    </xsd:attribute>

    <!-- Type of fractional damping algorithm -->
    <xsd:attribute name="fracAlg" use="optional" default="gl">
      <xsd:simpleType>
        <xsd:restriction base="xsd:token">
          <xsd:enumeration value="gl"/>
          <xsd:enumeration value="blank"/>
        </xsd:restriction>
      </xsd:simpleType>
    </xsd:attribute>

    <!-- Number of past time-steps for fractional damping -->
    <xsd:attribute name="fracMemory" type="xsd:positiveInteger" use="optional" default="1"/>

    <!-- Type of interpolation -->
    <xsd:attribute name="interpolation" use="optional" default="no">
      <xsd:simpleType>
        <xsd:restriction base="xsd:token">
          <xsd:enumeration value="no"/>
          <xsd:enumeration value="lin1pt"/>
        </xsd:restriction>
      </xsd:simpleType>
    </xsd:attribute>
  </xsd:complexType>


  <!-- ******************************************************************* -->
  <!--   Definition of syntax for specifying output quantities of CFS -->
  <!-- ******************************************************************* -->

  <!-- Definition of nodal result types of acoustic PDE -->
  <xsd:simpleType name="DT_AcousticNodeResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="acouPressure"/>
      <xsd:enumeration value="acouPotential"/>
      <xsd:enumeration value="acouPotentialD1"/>
      <xsd:enumeration value="acouPotentialD2"/>
      <xsd:enumeration value="lagrangeMultiplier"/>
      <xsd:enumeration value="acouRhsLoad"/>
      <xsd:enumeration value="meanFluidMechVelocity"/>
      <xsd:enumeration value="heatMeanTemperature"/>
      <xsd:enumeration value="splitLamb"/>
      <xsd:enumeration value="splitDivLamb"/>
<!--      <xsd:enumeration value="splitDivLambRhsLoad"/>-->
      </xsd:restriction>
  </xsd:simpleType>

  <!-- Definition of element result types of acoustic PDE -->
  <xsd:simpleType name="DT_AcousticElemResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="acouPressure"/>
      <xsd:enumeration value="acouIntensity"/>
      <xsd:enumeration value="acouSpeedOfSound"/>
      <xsd:enumeration value="acouVelocity"/>
      <xsd:enumeration value="acouPosition"/>
      <xsd:enumeration value="density"/>
      <xsd:enumeration value="pmlDampFactor"/>
      <xsd:enumeration value="divMeanFluidMechVelocity"/>
      <xsd:enumeration value="acouRhsLoadDensity"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- Definition of surface element result types of acoustic PDE -->
  <xsd:simpleType name="DT_AcousticSurfElemResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="acouSurfIntensity"/>
      <xsd:enumeration value="acouNormalIntensity"/>
      <xsd:enumeration value="acouNormalVelocity"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- Definition of surface region result types of acoustic PDE -->
  <xsd:simpleType name="DT_AcousticSurfRegionResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="acouPower"/>
      <xsd:enumeration value="acouPowerPlaneWave"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- Definition of region result types of magnetic PDE -->
  <xsd:simpleType name="DT_AcousticRegionResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="acouEnergy"/>
      <xsd:enumeration value="acouPotEnergy"/>
      <xsd:enumeration value="acouKinEnergy"/>
    </xsd:restriction>
  </xsd:simpleType>

  <!-- Definition of field variables (union of nodal and element results) -->
  <xsd:simpleType name="DT_AcousticSensorArrayResult">
    <xsd:union memberTypes="DT_AcousticNodeResult DT_AcousticElemResult"/>
  </xsd:simpleType>

  <!-- Global type for specifying desired electrostatic output quantities -->
  <xsd:complexType name="DT_AcousticStoreResults">
    <xsd:sequence>
      <xsd:choice maxOccurs="unbounded">

        <!-- Nodal result definition -->
        <xsd:element name="nodeResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_NodeResult">
                <xsd:attribute name="type" type="DT_AcousticNodeResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- Element result definition -->
        <xsd:element name="elemResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_ElemResult">
                <xsd:attribute name="type" type="DT_AcousticElemResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- Region result definition -->
        <xsd:element name="regionResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_RegionResult">
                <xsd:attribute name="type" type="DT_AcousticRegionResult"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- Surface result definition -->
        <xsd:element name="surfElemResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_SurfElemResult">
                <xsd:attribute name="type" type="DT_AcousticSurfElemResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- Surface region result definition -->
        <xsd:element name="surfRegionResult" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_SurfRegionResult">
                <xsd:attribute name="type" type="DT_AcousticSurfRegionResult" use="required"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

        <!-- SensorArrayResults -->
        <xsd:element name="sensorArray" minOccurs="0" maxOccurs="unbounded">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="DT_SensorArrayResult">
                <xsd:attribute name="type" type="DT_AcousticSensorArrayResult"/>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>

      </xsd:choice>
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="DT_DomainDimension">
    <xsd:attribute name="dimX" type="DT_PosFloat"/>
    <xsd:attribute name="dimY" type="DT_PosFloat"/>
    <xsd:attribute name="dimZ" type="DT_PosFloat"/>
  </xsd:complexType>
  <xsd:simpleType final="" name="DT_AcouImpedanceType">
    <xsd:annotation>
      <xsd:documentation>The existing type of impedance models:
slit_mpp
interpolImpedance</xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:string">
      <xsd:enumeration value="slit_mpp"/>
      <xsd:enumeration value="circ_mpp"/>
      <xsd:enumeration value="interpolImpedance"/>
      <xsd:enumeration value="fctImpedance"/>
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name="DT_Mpp">
    <xsd:sequence>
      <xsd:element name="holeDiam" type="xsd:double">
        <xsd:annotation>
          <xsd:documentation xml:lang="en">
            Hole Diameter or Slit width of MPP holes.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="plateThick" type="xsd:double">
        <xsd:annotation>
          <xsd:documentation xml:lang="en">
            Thickniss of MPP.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="mppVolDepth" type="xsd:double">
        <xsd:annotation>
          <xsd:documentation xml:lang="en">The volume depth behind the mpp
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="porosity" type="xsd:double">
        <xsd:annotation>
          <xsd:documentation xml:lang="en">
            Porosity of the perforated surface.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="flowMachNumber" type="xsd:double">
        <xsd:annotation>
          <xsd:documentation xml:lang="en">
            Mach number of flow.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element minOccurs="1" name="beta" type="xsd:double">
        <xsd:annotation>
          <xsd:documentation xml:lang="en">
            A factor for grazing flow effects.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:token" use="required"/>
    <xsd:attribute default="slit" form="unqualified" name="subtype" use="optional">
      <xsd:simpleType>
        <xsd:restriction base="xsd:token">
          <xsd:enumeration value="slit"/>
          <xsd:enumeration value="circ"/>
        </xsd:restriction>
      </xsd:simpleType>
    </xsd:attribute>
  </xsd:complexType>
  <xsd:complexType name="DT_InterpolImpedance">
    <xsd:sequence>
      <xsd:element name="dataName_real" type="xsd:string">
        <xsd:annotation>
          <xsd:documentation>Filename of interpolated Data for frequency dependant impedance -- real part.</xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="dataName_imag" type="xsd:string">
        <xsd:annotation>
          <xsd:documentation>Filename of interpolated Data for frequency dependant impedance -- imaginary part.</xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:token" use="required"/>
  </xsd:complexType>
  <xsd:complexType name="DT_FctImpedance">
    <xsd:sequence>
      <xsd:element name="fctReal" type="xsd:string"/>
      <xsd:element name="fctImag" type="xsd:string"/>
    </xsd:sequence>
    <xsd:attribute name="id" type="xsd:token" use="required"/>
  </xsd:complexType>
</xsd:schema>
