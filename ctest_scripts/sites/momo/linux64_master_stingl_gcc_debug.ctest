# This is a single test case to be called via nightly_test.cmake.
#
# You may test this script alone via ctest -S <test>.ctest
#
# see nightly_test. or https://cfs.mdmt.tuwien.ac.at/trac/wiki/NightlyBuilds for more help
# enable highlighting in kate via Tools->Highlighting->Other->CMake
#
#-----------------------------------------------------------------------------
# Set source and binary directories on the machine
#-----------------------------------------------------------------------------
SET(CTEST_SOURCE_DIRECTORY "$ENV{HOME}/code/master_stingl")
SET(CTEST_BINARY_DIRECTORY "$ENV{HOME}/builds/debug-master_stingl")

include("${CTEST_SOURCE_DIRECTORY}/ctest_scripts/shared/test_macros.cmake")

# set e.g. HOSTNAME
SET_GLOBAL_VARS()

IDENTIFY_DISTRO()
# empty binary directory
file(REMOVE_RECURSE "${CTEST_BINARY_DIRECTORY}")
file(MAKE_DIRECTORY "${CTEST_BINARY_DIRECTORY}")

# set the compiler environment
SET_COMPILER_ENV("GCC")

SET(BUILDTYPE "RELEASE")
SET(CTEST_BUILD_NAME "master_stingl ${DIST} ${REV} ${CXX_ID} ${CXX_VERSION} ${BUILDTYPE}")
set(CTEST_SITE "${HOSTNAME}") 

# will generate CTestConfig.cmake using our sites variant
WRITE_CTEST_CONFIG()

#-----------------------------------------------------------------------------
# Enter the following values into the initial cache
# ${CTEST_BINARY_DIRECTORY}/CMakeCache.txt before starting the configure
# run.
#-----------------------------------------------------------------------------
INIT_CACHE(CTEST_INITIAL_CACHE)
SET(CTEST_INITIAL_CACHE
  "${CTEST_INITIAL_CACHE}
   DEBUG:BOOL=ON
   CMAKE_BUILD_TYPE:STRING=${BUILDTYPE}
   TESTSUITE_DIR:STRING=$ENV{HOME}/testsuites/test-master_stingl
   TESTSUITE_PYTHON:BOOL=ON
   CFS_DEPS_PRECOMPILED:BOOL=ON
   CFS_DEPS_CACHE_DIR:PATH=$ENV{HOME}/caches/cache-master_stingl
   TESTSUITE_THREADS:STRING=1
   BUILD_CFSDAT:BOOL=ON
   USE_PARDISO:BOOL=ON
   USE_CGAL:BOOL=ON
   USE_CGNS:BOOL=ON
   USE_CCMIO:BOOL=OFF
   USE_FLANN:BOOL=OFF   
   USE_ENSIGHT:BOOL=OFF
   USE_FEAST:BOOL=ON
   USE_SNOPT:BOOL=ON
   USE_SCPIP:BOOL=ON
   USE_IPOPT:BOOL=ON
   USE_OPENMP:BOOL=OFF
   USE_ILUPACK:BOOL=ON
   TESTSUITE_CFSTOOL_MODE=relL2diff
   MKL_ROOT_DIR:PATH=/opt/intel/compilers_and_libraries/linux/mkl
   BUILDNAME:STRING=${CTEST_BUILD_NAME}")

DO_TESTING()
